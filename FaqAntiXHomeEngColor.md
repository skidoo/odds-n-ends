---
---

<div class="shmoo">

**AntiX 19 - F A Q**

<div id="Table of Contents1" dir="ltr">

<div id="Table of Contents1_Head" dir="ltr">

**Table of Contents**

</div>

[Home](#)

[Introduction](#)

[System requirements](#_system_requirements)

[Some Great Features in antiX](#)

[antiX-19](#)

[Pre installation](#)

[Installation](#)

[<span style="font-style: normal">Post-installation</span>](#)[31](#)

[Links](#)

</div>

Introduction {#introduction .western align="center"}
------------

![wm/default\_desktop.jpg](FaqAntiXHomeEngColor_files/FaqAntiXHomeEngColor_htm_3b3ce4cef632dfee.jpg){width="680"
height="384"}

<span>Instead of a heavy common Desktop Environment, antiX uses window
managers to control what the end-user can see and do. We hope these FAQs
will give you a basic orientation to antiX and its window managers, and
provide the means to explore further on your own.</span>

<span>antiX comes in four flavours for 32 and 64 bit boxes. antiX comes
as a </span>*<span>full</span>*<span> distro (c1GB), a
</span>*<span>base</span>*<span> distro (&lt;700MB), a
</span>*<span>core</span>*<span> distro (c310MB) and a
</span>*<span>net</span>*<span> distro (c150MB) all with a kernel that
will boot "antique" PII, PIII computers as well as the latest "modern"
processors.</span>

  ------------------------------------------------------------------------------------------------------------------------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  <span>![Note](FaqAntiXHomeEngColor_files/FaqAntiXHomeEngColor_htm_e082b12a2ecd0468.png){width="48" height="48"} </span>   <span>The </span>*<span>full</span>*<span> flavour will not fit on a cd. The </span>*<span>base</span>*<span> version fits on a cd, but does not include libroffice as well as some other applications.</span>
  ------------------------------------------------------------------------------------------------------------------------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

<span>By default, antiX loads into a Rox-IceWM desktop with a few icons
on the desktop. Use </span>*<span>F6</span>*<span> at the boot menu
screen to choose your desktop. What you choose running live will
automatically transfer if/when installed.</span>

<span>antiX is a very flexible linux distribution. You can run it live
from a cd, live from a usb stick (with persistence ie changes are saved
on reboot) as well as setting up a
</span>*<span>frugal-install</span>*<span> from an internal or external
hard drive. Of course, you can install to internal and external drives,
sticks, cards etc. You can even run it live, add/remove applications,
customize it, remaster it and then install. All your changes will carry
over to install!</span>

<span>antiX is based on </span>*<span>Debian</span>*<span> but is
totally free of </span>*<span>systemd</span>*<span>! It comes with a
custom kernel, its own custom scripts and repository to enhance user
experience. antiX can be used as a </span>*<span>rolling
release</span>*<span> distro ie you should be able to keep your
applications up to date by regularly upgrading. If you wish you can
enable the Debian testing or unstable repositories and live on the
</span>*<span>bleeding-edge</span>*<span>! For those that prefer
stability, keep to the Debian Stable/buster repositories.</span>

<span>A further feature of antiX is that you can install kernels from a
variety of sources including Debian, siduction, aptosid and liquorix.
This is especially useful if you have a new box as newer hardware is
more likely to be detected and work with newer kernels.</span>

<span>Don't forget - antiX is systemd free!</span>

[]() <span>System requirements</span> {#system-requirements .western align="center"}
-------------------------------------

### []() <span>So what are the minimum and suggested requirements to run antiX?</span> {#so-what-are-the-minimum-and-suggested-requirements-to-run-antix .western}

<span>antiX should run on most computers, ranging from 192MB old PII
systems with pre-configured 128MB swap to the latest powerful
boxes.</span>

*<span>antiX-core</span>*<span> and
</span>*<span>antiX-net</span>*<span> will run with </span>*<span>128MB
RAM plus swap</span>*<span>, but don't expect miracles!</span>

*<span>192MB RAM</span>*<span> is the </span>*<span>recommended
minimum</span>*<span> for antiX. </span>*<span>256MB RAM</span>*<span>
and above is </span>*<span>preferred</span>*<span> especially for
</span>*<span>antiX-full</span>*<span>.</span>

*<span>antiX-full</span>*<span> needs a </span>*<span>5GB</span>*<span>
minimum hard disk size. </span>*<span>antiX-base</span>*<span> needs
</span>*<span>3GB</span>*<span> and
</span>*<span>antiX-core</span>*<span> needs
</span>*<span>1GB.</span>*<span> </span>*<span>antiX-net</span>*<span>
needs </span>*<span>0.7GB.</span>*

### <span>Which flavour should I use?</span> {#which-flavour-should-i-use .western}

<span>Most users will be happy to use
</span>*<span>antiX-full</span>*<span> as it offers a full desktop
experience on legacy and modern computers.</span>

<span>If you have a very old desktop/laptop with 512MB RAM or less (PII,
PIII), or you want a desktop with "the basics", it is probably best to
use </span>*<span>antiX-base</span>*<span>.</span>

<span>If you want complete control over what applications to install and
know the Debian system fairly well, then use
</span>*<span>antiX-core</span>*<span> or
</span>*<span>antiX-net</span>*<span> since both these do not include
X.</span>

  ------------------------------------------------------------------------------------ -----------------------------------------------------------------------------------------------------------------
  <span>![Note](FaqAntiXHomeEngColor_files/note.png){width="48" height="48"} </span>   *<span>antiX-core</span>*<span> includes non-free firmware, which means that most wireless is supported.</span>
  ------------------------------------------------------------------------------------ -----------------------------------------------------------------------------------------------------------------

``` {.western}
```

### <span style="background: #ffffcc">PAE or non-PAE?</span> {#pae-or-non-pae .western}

<span style="background: #ffffcc">PAE stands for Physical Address
Extension, a way of allowing 32 bit operating systems to access ram
beyond around 4GB. It is possible to use a non-PAE version on a PAE
system, but not vice versa. </span>

<span style="background: #ffffcc">anti</span><span
style="background: #ffffcc">X Linux is available for two architectures:
[32bit](https://en.wikipedia.org/wiki/32-bit) and
[64bit](https://en.wikipedia.org/wiki/64-bit_computing). </span>

<span style="background: #ffffcc">The 32 bit version uses a non-pae
kernel.</span>

<span style="background: #ffffcc">If unsure whether you need the PAE or
non-PAE version, use the method below suitable for the OS you currently
run. </span>

-   <span style="background: #ffffcc">Linux. Open a terminal and enter
    this command (install **inxi** first if necessary): *inxi -f*. If
    the CPU Flags entry does not include PAE in the list, then it needs
    the non pae version (32 bit). </span>
-   <span style="background: #ffffcc">Mac. Intel versions of OS X
    support PAE. </span>
-   <span style="background: #ffffcc">Windows^®^</span>

    -   <span style="background: #ffffcc">Windows2000 and earlier:
        non-PAE </span>
    -   <span style="background: #ffffcc">Windows XP and Vista. Right
        click My Computer &gt; Properties, General tab. If it says
        Physical Address Extension (=PAE) at the bottom, then PAE is the
        correct version to install. </span>
    -   <span style="background: #ffffcc">Windows 7. Open the Command
        Prompt window by clicking the Start button &gt; All
        Programs &gt; Accessories &gt; Command Prompt. A terminal window
        will appear. Enter this code at the command prompt where the
        cursor is positioned: </span>

        <span style="background: #ffffcc">If PAE is enabled, you will
        get a return like this: *PAEEnabled.* That return may or may not
        be followed by the word TRUE. </span>

    -   <span style="background: #ffffcc">Windows 8 and later. PAE
        enabled by default. </span>

### <span style="background: #ffffcc">32 or 64 bit? </span> {#or-64-bit .western}

#### []()<span style="background: #ffffcc">What is the architecture of your cpu? </span> {#what-is-the-architecture-of-your-cpu .western}

<span style="background: #ffffcc">Follow the appropriate method below to
find out whether your machine is 32- or 64-bit.\* </span>

-   <span style="background: #ffffcc">**Linux**. Open a terminal and
    enter the command ***lscpu***, then examine the first few lines for
    architecture, number of cores, etc. </span>
-   **<span style="background: #ffffcc">Windows</span>**<span
    style="background: #ffffcc">. Consult [this Microsoft
    document](https://support.microsoft.com/en-us/kb/827218). </span>
-   **<span style="background: #ffffcc">Apple</span>**<span
    style="background: #ffffcc">. Consult [this Apple
    document](https://support.apple.com/en-us/HT201948). </span>

<span style="background: #ffffcc">\*If you want to know the architecture
of the OS instead, the command ***uname -m*** will probably work on all
platforms. </span>

<span style="background: #ffffcc">In general, if you have a 64-bit cpu
and the required RAM for your particular machine and processor, you
should use the 64-bit version. This is because 64-bit is generally
faster, though you may not actually notice the difference in daily use.
In the long run, moreover, an increasing number of larger applications
will likely be restricted to 64-bit versions. Note that a 32-bit
application or OS can run on a 64-bit cpu, but not the reverse. </span>

<span style="background: #ffffcc">MORE:
[here](https://www.techsupportalert.com/content/32-bit-and-64-bit-explained.htm)
</span>

#### []()<span style="background: #ffffcc">How much memory (RAM) do you have? </span> {#how-much-memory-ram-do-you-have .western}

-   <span style="background: #ffffcc">Linux. Open a terminal and enter
    the command ***free -h*** and look at the number in the
    Total column. </span>
-   <span style="background: #ffffcc">Windows. Open the System window
    using whatever method is recommended for your version, and look for
    the entry “Installed memory (RAM).” </span>
-   *<span style="background: #ffffcc">Apple. Click the entry "About
    this Mac" in the Apple menu on Mac OS X and look for the
    RAM information. </span>*

### <span>I have an old laptop with very low RAM, what should I do?</span> {#i-have-an-old-laptop-with-very-low-ram-what-should-i-do .western}

<span>If you have less than 512MB RAM, and want to test antiX live,
choose one of the </span>*<span>min-</span>*<span> options at
</span>*<span>F6</span>*<span>. To install to hard drive, at the live
boot menu, type 3, login as root and type cli-installer.</span>

*<span>It is also a good idea to create a swap partition before
installation.</span>*

<span>Some Great Features in antiX</span> {#some-great-features-in-antix .western align="center"}
-----------------------------------------

### []()<span>live-usb-maker</span> {#live-usb-maker .western}

<span>Install the downloaded or newly created iso file to a portable usb
stick nd take your antiX with you in your pocket! Includes an option to
encrypt the usb device for security. This tool inludes a basic gui front
end, but the cli version offers more options and is easy to use.</span>

### []()<span>package-installer:</span> {#package-installer .western}

<span>Located in Menu-→ Applications-→System Tools -→ package-installer
You can install packages for Disk-Recovery, Web Browsers, Graphics,
Kids, Language, LaTex, Network, Non-free, Office, Server, WindowManager
. . . simply choose the package you want, and the installer will do the
rest. (internet connection required)</span>

### []()<span>LuckyBackup:</span> {#luckybackup .western}

<span>Can be found in Control Centre-→ Disks-→ Backup Your System or in
Menu-→ Applications-→System Tools-→ luckyBackup</span>

### []()<span>Repo Manager</span> {#repo-manager .western}

<span>This tool greatly simplifies the process of changing the package
repositories that are used for updating/upgrading applications. The
sources are set during installation depending on the selected timezone
and language, and will generally work well. But there may be instances
where a user might wish to change those selections.</span>

### []()<span>iso-snapshot:</span> {#iso-snapshot .western}

<span>Want to make a live iso backup of what you have installed on your
hard drive? Then, this is for you! Simple, but effective.</span>

### []()<span>Remaster and Persistence:</span> {#remaster-and-persistence .western}

<span>Not only have we made it easy to set up antiX live with
persistence, we also make it easy to create a remaster of the running
live system!</span>

### []()<span>Customised gfxboot menu:</span> {#customised-gfxboot-menu .western}

<span>Use the </span>*<span>F</span>*<span> keys to set up how you want
antiX to boot in live mode and </span>*<span>F8</span>*<span> to save
the changes for future live boots!</span>

<div style="padding-left:32px;">

<span>\*F1 - Help</span>

<span>\*F2 - Language</span>

<span>\*F3 - Time zone</span>

<span>\*F4 - Various hardware options</span>

<span>\*F5 - Persistence options</span>

<span>\*F6 - Desktop and font size options</span>

<span>\*F7 - Console resolution options</span>

<span>\*F8 - Save changes (only on writable media)</span>

</div>

### []() <span>I don't like that splash image. It takes up too much of my small screen space. How do I boot without it?</span> {#i-dont-like-that-splash-image.-it-takes-up-too-much-of-my-small-screen-space.-how-do-i-boot-without-it .western}

<span>Easy. Just press </span>*<span>F7</span>*<span> console
(</span>*<span>F6</span>*<span> on core or net) and choose the
</span>*<span>default</span>*<span> option.</span>

### []() <span>What do you mean by Safe Video Mode and Failsafe in the menu?</span> {#what-do-you-mean-by-safe-video-mode-and-failsafe-in-the-menu .western}

<span>Safe Video Mode Disable KMS (kernel mode set) video drivers and
force the use of the vesa video driver. Try this option of the system
seems to boot but the screen is blank.</span>

<span>Failsafe Boot In addition to forcing safe video, also load all
drivers early in the boot process. Try this option if the system does
not boot at all.</span>

\

------------------------------------------------------------------------

\
[]()<span>antiX-19</span> {#antix-19 .western align="center"}
-------------------------

### []()So, what's new in antiX-19? {#so-whats-new-in-antix-19 .western}

#### Lots! Explore! {#lots-explore .western}

-   <span>Based on Debian Buster, but without systemd and libsystemd0.
    </span>
-   <span>eudev 3.2.8 replaces udev </span>
-   <span>Customised 4.9193 kernel with fbcondecor splash </span>
-   <span>libreoffice 6.1.5-3 </span>
-   <span>firefox-esr 60.9.0esr-1 </span>
-   <span>claws-mail 3.17.3-2 </span>
-   <span>cups for printing </span>
-   <span>xmms -for audio </span>
-   <span>gnome-mplayer - for playing video </span>
-   <span>smtube - play youtube videos without a using a browser </span>
-   <span>streamlight-antix - stream videos with very low RAM usage.
    </span>
-   <span>qpdfview - pdf reader </span>
-   <span>arc-evopro2-theme-antix </span>

#### File managers and desktop: {#file-managers-and-desktop .western}

-   <span>spacefm </span>
-   <span>rox-filer </span>

#### Convert your video and audio files with: {#convert-your-video-and-audio-files-with .western}

-   <span>winff </span>
-   <span>asunder </span>

#### Connect to the net with: {#connect-to-the-net-with .western}

-   <span>connman </span>
-   <span>or gnome-ppp if you are still on dial-up </span>

#### Editors: {#editors .western}

-   <span>geany </span>
-   <span>leafpad </span>
-   <span>Midnight Commander </span>

#### Tools for remastering and creating snapshots of installed system: {#tools-for-remastering-and-creating-snapshots-of-installed-system .western}

-   <span>iso-snapshot </span>
-   <span>remaster tools </span>

#### General tools: {#general-tools .western}

-   <span>bootrepair </span>
-   <span>codecs installer </span>
-   <span>Network Assistant </span>
-   <span>User Manager </span>
-   <span>ddm-mx - install nvidia drivers </span>

#### Others: {#others .western}

-   <span>hexchat - gui chat </span>
-   <span>luckybackup - excellent backup tool. There's nothing lucky
    about it! </span>
-   <span>simple-scan - for scanning documents </span>
-   <span>transmission-gtk - torrent downloader </span>
-   <span>wingrid-antix - turn the stacking window managers into tilers.
    </span>
-   <span>Xfburn for burning cd/dvd </span>
-   <span>connectshares-antix for network shares </span>
-   <span>droopy-antix - an easy way to transfer files over the net.
    </span>
-   <span>mirage - image viewer </span>
-   <span>package-installer - install applications easily and safely
    </span>
-   <span>antiX Control Centre - an easy way to do just about anything!
    </span>
-   <span>streamtuner2 - listen to streaming radio </span>
-   <span>cherrytree - note taking application </span>

#### <span>Why not try out our included </span>*<span>cli</span>*<span> apps:</span> {#why-not-try-out-our-included-cli-apps .western}

-   <span>Editors: nano and vim </span>
-   <span>Newsreader: newsboat </span>
-   <span>Chat: irssi </span>
-   <span>Audio player: mocp </span>
-   <span>Radio: pmrp </span>
-   <span>Video player: mpv </span>
-   <span>Youtube video: mps-youtube </span>
-   <span>Audio ripper: abcde </span>
-   <span>Torrent: rtorrent </span>
-   <span>Cd burner: cdw </span>
-   <span>Writer: Wordgrinder </span>

**<span>NEW</span>**

-   <span>cli-aptiX </span>
-   <span>live-kernel-updater </span>
-   <span>lxkeymap </span>
-   <span>fskbsetting </span>
-   <span>backlight-brightness </span>
-   <span>antiX-cli-cc </span>

#### Cool in-house antiX apps available in the repos: {#cool-in-house-antix-apps-available-in-the-repos .western}

-   <span>1-to-1-voice-antix - Voice chat between two pcs via encrypted
    mumble </span>
-   <span>1-to-1-assistance-antix - Remote access help application
    </span>
-   *<span>ssh-conduit - Remote resouces via an ssh encypted connection
    </span>*

### What kernel is antiX-19 using? {#what-kernel-is-antix-19-using .western}

A customised 4.9.193 version.

There are several other kernels available via the *package-installer*
application. Such as:

-   custom-4.19.73 for 32 and 64 bit processors, pae and non-pae for
    stable/buster, testing and sid.
-   *<span>custom-5.2.15 for 32 and 64 bit processors, pae and non-pae
    for stable/buster, testing and sid. </span>*

*<span style="background: #ffffcc">Pre installation</span>* {#pre-installation .western align="center"}
-----------------------------------------------------------

### <span style="background: #ffffcc">*<span style="font-style: normal"><span style="background: #ffffff">The Live System</span></span>*</span> {#the-live-system .western}

<span style="background: #ffffcc">An antiX Linux LiveMedium (USB or DVD)
boots your computer without accessing the hard disk. It copies a virtual
file system into RAM that acts as the center of a temporary operating
system for the computer. When you end your Live session, everything
about your computer is back to the way it was, unchanged. </span>

<span style="background: #ffffcc">This provides a number of benefits:
</span>

-   <span style="background: #ffffcc">It enables you to run antiX Linux
    on your computer without installing it. </span>
-   <span style="background: #ffffcc">It allows you to determine whether
    antiX Linux is compatible with your hardware. </span>
-   <span style="background: #ffffcc">It helps you to get a feel for how
    antiX Linux works and to explore some of its features. </span>
-   <span style="background: #ffffcc">You can decide whether antiX Linux
    is what you want without permanently affecting your current system.
    </span>

<span style="background: #ffffcc">Running from a LiveDVD also has some
disadvantages: </span>

-   <span style="background: #ffffcc">Because the entire system is
    operating from a combination of RAM and the medium, antiX Linux will
    require more RAM and run more slowly than if it were installed on
    the hard drive. </span>
-   *<span style="background: #ffffcc">Some unusual hardware that
    requires specialized drivers or custom configuration may not work in
    a LiveMedium session where permanent files can't be installed.
    Installing and removing software is also not possible because the
    DVD is a read-only medium. </span>*

### <span style="background: #ffffcc">Creating a bootable medium</span> {#creating-a-bootable-medium .western}

\

#### *<span style="background: #ffffcc">Obtain the ISO </span>* {#obtain-the-iso .western}

<span style="background: #ffffcc">anti</span><span
style="background: #ffffcc">X Linux is distributed as an ISO, a disk
image file in the </span>[<span style="font-weight: normal"><span
style="background: #ffffcc">ISO
9660</span></span>](http://en.wikipedia.org/wiki/ISO_9660)<span
style="background: #ffffcc"> file system format. </span>

<span style="background: #ffffcc">**Download** </span>

<span style="background: #ffffcc">anti</span><span
style="background: #ffffcc">X Linux can be downloaded in two ways from
[the Download page](https://antixlinux.com/download/). </span>

-   **<span style="background: #ffffcc">Direct</span>**<span
    style="background: #ffffcc">. Click on the link to [the ISO Download
    page](https://antixlinux.com/download/). Select the mirror you want
    to use, then c</span><span style="background: #ffffcc">lick on the
    correct link for your architecture and mode</span><span
    style="background: #ffffcc">.</span><span
    style="background: #ffffcc"> </span><span
    style="background: #ffffcc">S</span><span
    style="background: #ffffcc">ave the ISO to your Hard Disk. If one
    source seems slow, try the other one. </span>
-   **<span style="background: #ffffcc">Torrent</span>**<span
    style="background: #ffffcc">.
    [BitTorrent](http://en.wikipedia.org/wiki/BitTorrent) file sharing
    provides an internet protocol for efficient mass transfer of data.
    It decentralizes the transfer in such a way as to utilize good
    bandwidth connections and to minimize strain on
    low-bandwidth connections. An added benefit is all BitTorrent
    clients perform error checking during the download process, so there
    is no need to do a separate md5sum check after your download
    is complete. It has already been done! </span>

    <span style="background: #ffffcc">Links to the torrents will be on
    [the Download page](https://antixlinux.com/torrent-files/).</span>

<!-- -->

-   <span style="background: #ffffcc">Go to the torrent download page
    and click on the Torrent button that you want to use <span
    lang="en-US">according to your architecture</span> (32-bit
    or 64-bit). Your browser should recognize that it is a torrent,
    <span lang="en-US">so it will open a popup window asking you what
    you want to do with it (download or open it). Choose
    to download.</span></span>
-   <span style="background: #ffffcc">A file with the ISO name and with
    the .torrent extension will be downloaded to your "Download"
    folder</span>

    <span style="background: transparent">*<span
    style="font-style: normal"><span
    style="background: #ffffcc">Clicking on the downloaded torrent
    </span></span><span style="font-style: normal"><span
    style="background: #ffffcc">will </span></span><span
    style="font-style: normal"><span style="background: #ffffcc">launch
    your </span></span><span style="font-style: normal"><span
    style="background: #ffffcc">torrent client (Transmission
    by default)</span></span><span style="font-style: normal"><span
    style="background: #ffffcc">,</span></span><span
    style="font-style: normal"><span style="background: #ffffcc">
    show</span></span><span style="font-style: normal"><span
    style="background: #ffffcc">ing</span></span><span
    style="font-style: normal"><span style="background: #ffffcc"> the
    torrent in its list; highlight it and click Start to begin the
    download process.</span></span>*</span>

#### <span style="background: #ffffcc">Check validity of downloaded ISOs</span> {#check-validity-of-downloaded-isos .western}

<span style="background: #ffffcc">After you have downloaded an ISO, the
next step is to verify it. There are several methods available. </span>

[]() **<span style="background: #ffffcc">Md5sum</span>**

\

<span style="background: #ffffcc">Each ISO is accompanied by a matching
md5sum file in the source, and you should check its **md5sum** against
the official one. It will be identical to the official md5sum if your
copy is authentic. The following steps will let you verify the integrity
of the downloaded ISO on any OS platform. </span>

-   <span style="background: #ffffcc">Windows</span>

    <span style="background: #ffffcc">Users can check </span><span
    style="background: #ffffcc">most easily with the
    [Rufus](https://rufus.akeo.ie/) bootable USB maker; a</span><span
    style="background: #ffffcc"> tool called
    [WinMD5FREE](http://www.winmd5.com/)</span><span
    style="background: #ffffcc"> is also available to download and use
    free of cost</span><span style="background: #ffffcc">. </span>

-   <span style="background: #ffffcc">Linux </span>

    <span style="background: #ffffcc">In antiX Linux, navigate to the
    folder where you have downloaded the ISO and the md5sum file.
    Right-click the md5sum file &gt; Check data integrity. A dialog box
    will pop up saying “&lt;name of ISO&gt;: OK” if the numbers
    are identical. You can also right-click the ISO &gt; Compute md5sum
    and compare it with another source. </span>

    <span style="background: #ffffcc">For situations where that option
    is not available, open a terminal in the location where you
    downloaded the ISO (in Thunar: File &gt; Open Terminal Here), then
    type: </span>

    <span style="background: #ffffcc">Be sure to replace “filename” with
    the actual filename (type in the first couple of letters then hit
    Tab and it will be filled in automatically). Compare the number
    obtained by this calculation with the md5sum file downloaded from
    official site. If they are identical, your copy is identical to the
    official release. </span>

-   <span style="background: #ffffcc">Mac </span>

    <span style="background: #ffffcc">Mac users need to open up a
    console/terminal and change into the directory with the ISO and
    md5sum files. Then issue this command: </span>

    <span style="background: #ffffcc">Be sure to replace filename with
    the actual filename. </span>

**<span style="background: #ffffcc">sha256sum</span>**

<span style="background: #ffffcc">antiX allows you to take advantage of
the security system provided </span><span
style="background: #ffffcc">with </span>[<span
style="background: #ffffcc">sha256 and
sha512</span>](https://en.wikipedia.org/wiki/SHA-2)<span
style="background: #ffffcc"> .</span><span style="background: #ffffcc">
Download the file to check the integrity of the ISO. </span>

-   <span style="background: #ffffcc"><span
    style="font-weight: normal">W</span><span
    style="font-weight: normal">indows: the method varies by version. Do
    a web search on "</span>*<span style="font-weight: normal">windows
    </span><span style="font-weight: normal">&lt;</span><span
    style="font-weight: normal">version&gt; check sha256
    sum</span>*<span style="font-weight: normal">"</span></span>
-   <span style="font-weight: normal"><span
    style="background: #ffffcc">Linux</span></span><span
    style="font-weight: normal"><span
    style="background: #ffffcc">:</span></span><span
    style="font-weight: normal"><span style="background: #ffffcc">
    follow the directions for md5sum, above</span></span><span
    style="font-weight: normal"><span style="background: #ffffcc">,
    substituting </span></span>**<span
    style="background: #ffffcc">"sha256sum"</span>**<span
    style="background: #ffffcc"> or </span>**<span
    style="background: #ffffcc">"sha512sum"</span>**<span
    style="background: #ffffcc"> </span><span
    style="font-weight: normal"><span style="background: #ffffcc"> for
    "md5sum."</span></span>
-   <span style="background: #ffffcc">Mac: open a console, change to the
    directory with the ISO and sha256 files, and issue this
    command:</span>

``` {.western}
     shasum -a 256 /path/to/file
```

\
\

[]() <span style="background: #ffffcc">**GPG signature** </span>

<span style="background: transparent">*<span
style="font-style: normal"><span
style="background: #ffffcc">anti</span></span><span
style="font-style: normal"><span style="background: #ffffcc">X ISO files
to be downloaded have been signed by their developers. This security
method allows the user to be confident that the ISO is what is says it
is: an official ISO from the developer. Detailed instructions about how
to run this security check can be found in the </span></span>[MX/antiX
Technical Wiki](https://mxlinux.org/wiki/system/signed-iso-files)<span
style="font-style: normal"><span style="background: #ffffcc">.
</span></span>*</span>

### *<span style="background: #ffffcc">Create the LiveMedium</span>* {#create-the-livemedium .western}

#### <span style="background: #ffffcc">DVD </span> {#dvd .western}

<span style="background: #ffffcc">Burning an ISO to a DVD is easy, as
long as you follow some important guidelines. </span>

-   <span style="background: #ffffcc">Do not burn the ISO onto a blank
    CD/DVD as if it were a data file! An ISO is a formatted and bootable
    image of an OS. You need to choose **Burn disk image** or **Burn
    ISO** in the menu of your CD/DVD burning program. If you just drag
    and drop it into a file list and burn it as a regular file, you will
    not get a bootable LiveMedium. </span>

#### []()<span style="background: #ffffcc">**USB** </span> {#usb .western}

<span style="background: #ffffcc">You can easily create a bootable USB
that works on most systems. antiX Linux includes the tool **Live**
**USB** **** **Maker** **(gui)** for this work. </span>

-   <span style="background: #ffffcc">If you want to create a USB on a
    Windows base, we suggest you use Rufus, which supports our
    bootloader, or a recent Unetbootin version (post 625). </span>
-   <span style="background: #ffffcc">If on a Linux base, be sure to
    reload your repos in Synaptic or antiX Updater in order to upgrade
    files such as syslinux and extlinux to the most recent versions.
    </span>
-   <span style="background: #ffffcc">If your USB starts but leaves you
    with an error message: *gfxboot.c32: not a COM32R image,* you should
    still be able to boot by typing "live" at the prompt in the
    next line. Reformatting the USB and reinstalling the ISO should
    remove the error. </span>
-   <span style="background: #ffffcc">If the graphic USB creators fail,
    it is possible to use the command "dd," an option now in
    Live-usb maker. </span>

    -   <span style="background: #ffffcc">WARNING: be careful to
        identify your destination USB correctly in the dd command line
        string listed above, as the dd command will completely write
        over the destination.</span>
    -   <span style="background: #ffffcc">To ascertain the correct
        device name/letter for your destination USB, open a terminal,
        type *lsblk*<span style="font-style: normal"> and press
        Enter</span>.</span>

        <span style="background: #ffffcc">A list of all devices
        connected to your system will be listed. You should be able to
        identify your destination USB by its listed storage size.
        </span>

-   <span style="background: #ffffcc">For details, see [the MX/antiX
    Wiki](https://mxlinux.org/wiki/system/dd-command).</span><span
    style="background: #ffffcc"> </span>

<span id="Frame16" dir="ltr"
style="float: left; width: 4.09in; height: 1.61in; border: none; padding: 0in; background: #ffffff"></span>

![](FaqAntiXHomeEngColor_files/FaqAntiXHomeEngColor_htm_1470804fe53dd569.png){width="100%"}
*<span style="display: none">\
</span>*\

*<span style="background: transparent">***<span
style="background: #ffffcc">typical output of the command lsblk, showing
two harddisks each with two partitions</span>***</span>*

\

*<span style="background: #ffffcc">I</span><span style="background: #ffffcc">nstallation</span>* {#installation .western align="center"}
------------------------------------------------------------------------------------------------

\

### *<span style="background: #ffffcc">Hard drive FAQs </span>* {#hard-drive-faqs .western}

#### []()<span style="background: #ffffcc">Where should I install antiX Linux? </span> {#where-should-i-install-antix-linux .western}

<span style="background: #ffffcc">Before starting the install, you need
to decide where you are going to install antiX. </span>

-   <span style="background: #ffffcc">Entire hard drive </span>
-   <span style="background: #ffffcc">Existing partition on a hard drive
    </span>
-   <span style="background: #ffffcc">New partition on a hard drive
    </span>

<span style="background: #ffffcc">You can simply select one of the first
two during installation, but the third requires the creation of a new
partition. You can do this during the antiX installation, but it is
recommended that you do it before you start the installation. In Linux,
you will usually be using **GParted**, a useful and very powerful tool.
</span>

<span style="background: #ffffcc">A traditional installation format for
Linux has three partitions, one each for root, home and Swap, as in the
Figure below, and you should begin with this if you are new to Linux.
Other partition arrangements are possible, for example some experienced
users combine root and home, with a separate partition for data. </span>

<span
style="background: #ffffcc">![](FaqAntiXHomeEngColor_files/FaqAntiXHomeEngColor_htm_399374155a3cb469.jpg){width="517"
height="279"} </span>

***Gparted showing* *three* *partitions* *(sda1), (sda2)* *and swap
(sda3)***

*Note that the drive shown* *here* *is* *also* *used for testing so that
the partitions are larger than* *normally* *needed.*

***<span style="background: #ffffcc">MORE: [GParted
Manual](http://gparted.org/display-doc.php?name=help-manual) </span>***

![](FaqAntiXHomeEngColor_files/FaqAntiXHomeEngColor_htm_c67ce670e83be805.png){width="65"
height="53"} <span style="background: #ffffcc"> </span>\

<span style="background: #ffffcc"> </span><span
style="font-style: normal"><span style="font-weight: normal"><span
style="background: #ffffcc">[Create a new partition with
GParted](https://www.youtube.com/watch?v=lf8eXhCKghg)
</span></span></span>

![](FaqAntiXHomeEngColor_files/FaqAntiXHomeEngColor_htm_217de41000d84f14.png){width="65"
height="53"}\

<span style="background: #ffffcc"> </span>[<span
style="font-style: normal"><span style="font-weight: normal"><span
style="background: #ffffcc">Partition a Multi-boot
system</span></span></span>](https://www.youtube.com/watch?v=khg6_sdrOBQ)

[<span style="background: #ffffcc">
</span>](https://www.youtube.com/watch?v=khg6_sdrOBQ)\

#### []()<span style="background: #ffffcc">How can I edit partitions? </span> {#how-can-i-edit-partitions .western}

<span style="background: #ffffcc">A very handy tool for such actions is
**Control Centre** **&gt;** **Disks** **&gt;** **Manage** **Disks**.
This utility provides a graphical presentation of all the partitions on
the machine (excluding swap) with a simple interface for quickly and
easily <span style="font-weight: normal">m</span>ounting, unmounting and
editing some properties of disk partitions. Changes are automatically
and immediately written to /etc/fstab and are thus preserved for the
next boot. </span>

<span style="background: #ffffcc">Disk Manager automatically allocates
mount points in /media, using /media/LABEL (e.g., /media/HomeData) if
the partition is labeled or /media/DEVICE (e.g., /media/cdrom) if not.
These mount points are created by DM when a partition is mounted, and
removed immediately when a partition is unmounted. </span>

<span style="background: #ffffcc">HELP: [Disk Manager
help.](http://mxlinux.org/wiki/help-files/help-disk-manager) </span>

#### []()<span style="background: #ffffcc">**What are those other partitions on my Windows installation?** </span> {#what-are-those-other-partitions-on-my-windows-installation .western}

<span style="background: #ffffcc">Recent home computers with Windows are
sold with a diagnostic partition and restore partition, in addition to
the one that contains the OS installation. If you see multiple
partitions showing up in GParted that you were not aware of, they are
probably those and should be left alone. </span>

#### []()<span style="background: #ffffcc">Should I create a separate Home? </span> {#should-i-create-a-separate-home .western}

<span style="background: #ffffcc">You do not have to create a separate
home, since the Installer will create a /home partition within / (root).
But having it separate makes upgrades easier and protects against
problems caused by users filling up the drive with a lot of pictures,
music, or video data. </span>

#### []()<span style="background: #ffffcc">Do I need to create a SWAP file? </span> {#do-i-need-to-create-a-swap-file .western}

<span style="background: #ffffcc">The Installer will create a SWAP file
for you. If you intend to hibernate (and not just suspend) the system
here are the recommendations for the size of the swap space:</span>

-   <span style="background: #ffffcc">For less then 1GB of physical
    memory (RAM), the swap space should at least be equal to the amount
    of RAM and a maximum twice the amount of RAM depending upon the
    amount of hard disk space available for the system.</span>
-   <span style="background: #ffffcc">For more systems with larger
    amounts of RAM, your swap space should at least be equal to the
    memory size. </span>

<span style="background: #ffffcc">Users with an SSD often avoid setting
up a SWAP file on the SSD to avoid slowing it down. </span>

#### []()<span style="background: #ffffcc">What do names like “sda” mean? </span> {#what-do-names-like-sda-mean .western}

<span style="background: #ffffcc">Before you begin installation, it is
critical that you understand how Linux operating systems treat hard
drives and their partitions. </span>

-   **<span style="background: #ffffcc">Drive names</span>**<span
    style="background: #ffffcc">. Unlike Windows, which assigns a drive
    letter to each of your hard drive partitions, Linux assigns a short
    device name to each hard drive or other storage device on a system.
    The device names always start with </span>**<span
    style="background: #ffffcc">sd</span>**<span
    style="background: #ffffcc"> plus a single letter. For instance, the
    first drive on your system will be sda, the second sdb, etc. There
    are also more advanced means of naming drives, the most common of
    which is the
    [UUID](https://en.wikipedia.org/wiki/Universally_unique_identifier)
    (Universally Unique IDentifier), used to assign a permanent name
    that will not be changed by the addition or removal of equipment.
    </span>
-   <span style="background: #ffffcc">**Partition names**. Within each
    drive every partition is referred to as a number appended to the
    device name. Thus, for instance, **sda1** would be the first
    partition on the first hard drive, while **sdb3** would be the third
    partition on the second drive. </span>
-   <span style="background: #ffffcc">**Extended partitions**. PC hard
    disks were originally permitted only four partitions. These are
    called primary partitions in Linux and are numbered 1 to 4. You can
    increase the number by making one of the primary partitions into an
    extended partition, then dividing that into logical partitions
    (limit 15) that are numbered from 5 onward. Linux can be installed
    into a primary or logical partition. </span>

### []()<span style="background: #ffffcc"> First look </span> {#first-look .western}

![](FaqAntiXHomeEngColor_files/FaqAntiXHomeEngColor_htm_5dbe99ea76ae090a.gif)
> <span style="background: #ffffcc">**Live Medium** **login** </span>

In case you want to log out and back in, install new packages, etc.,
here are the usernames and passwords:

\

-   Regular user

    -   name: demo
    -   password: demo

\

-   Superuser (Administrator)

    -   name: root
    -   password: root

### <span style="background: #ffffcc">Boot the LiveMedium </span> {#boot-the-livemedium .western}

#### []()<span style="background: #ffffcc">LiveCD/DVD </span> {#livecddvd .western}

<span style="background: #ffffcc">Simply place the DVD in the tray and
reboot <span lang="en-US">(it is assumed that the pc is already
configured to boot from cd-rom)</span>. </span>

#### []()<span style="background: #ffffcc">LiveUSB </span> {#liveusb .western}

<span style="background: #ffffcc">You may need to take a few steps to
get your computer to boot correctly using the USB. </span>

-   <span style="background: #ffffcc">To boot with the USB Drive, many
    computers have special keys you can press during booting to select
    that device. Typical Boot Device Menu keys are Esc, one of the
    Function keys (F2, F8, F12) or the Shift key. Look carefully at the
    first screen that shows up when rebooting to find the correct key.
    </span>
-   <span style="background: #ffffcc">Alternatively, You may have to go
    into the BIOS to change the boot device order: </span>

    -   <span style="background: #ffffcc">Boot the computer, and hit the
        required key (e.g., F2, F10 or Esc) at the beginning to get into
        the BIOS </span>
    -   <span style="background: #ffffcc">Click on (or arrow over to)
        the Boot tab </span>
    -   <span style="background: #ffffcc">Identify and highlight your
        USB device (usually, USB HDD), then move it to the top of the
        list (or enter, if your system is set for that). Save and exit
        </span>
    -   <span style="background: #ffffcc">If unsure or uncomfortable
        about changing the BIOS, ask for assistance in the Forums.
        </span>
-   <span style="background: #ffffcc">On older computers without USB
    support in the BIOS, you can use the [Plop Linux
    LiveCD](http://www.plop.at/) that will load USB drivers and present
    you with a menu. See the website for details. </span>
-   <span style="background: #ffffcc">Once your system is set to
    recognize the USB Drive during the boot process, just plug in the
    Drive and reboot the machine. </span>

#### []()<span style="background: #ffffcc">UEFI </span> {#uefi .western}

<span
style="background: #ffffcc">![](FaqAntiXHomeEngColor_files/FaqAntiXHomeEngColor_htm_69d4d1763127befc.png){width="65"
height="53"} </span> *video*: [<span style="background: #ffffcc">UEFI
Boot Issues, and some settings to check!
</span>](https://www.youtube.com/watch?v=cLFUZ6LtqiE)

<span style="background: #ffffcc">If the machine already has Windows 8
or later installed, then special steps must be taken to deal with the
presence of
[(U)EFI](https://en.wikipedia.org/wiki/Unified_Extensible_Firmware_Interface)
and Secure Boot. </span>

<span style="background: #ffffcc">Unified Extensible Firmware Interface
(UEFI) is a new kind of system firmware used on recent machines. It
represents the successor of old BIOS.</span>

<span style="background: #ffffcc"><span lang="en-US">Many PCs (but not
the most recent) that use UEFI have the so-called CSM ("Compatibility
Support Module") in the firmware, which provides the same interfaces to
an operating system as a classic PC BIOS, so that software written for
classic BIOS can be used unchanged on UEFI </span><span
lang="en-US">PC.</span></span>

<span style="background: #ffffcc">BIOS and UEFI use different
partitioning schemes:</span>

<span style="background: #ffffcc">Native UEFI uses a partitioning scheme
called GPT ("GUID Partition Table").</span>

<span style="background: #ffffcc">BIOS and UEFI in CSM mode, use the
Master Boot Record (MBR) partitioning scheme.</span>

<span style="background: #ffffcc">A UEFI - CSM computer will use the CSM
mode and therefore the MBR partitioning scheme if the Legacy function is
chosen, in the UEFI settings windows, at startup.</span>

<span style="background: #ffffcc">Another important difference between
BIOS (or UEFI in CSM mode) and UEFI in native mode is the location where
boot code is stored and in which format it has to be. </span>

<span style="background: #ffffcc"><span lang="en-US">There will need to
be a</span> specific bootloader for UEFI and a different one for BIOS
(or UEFI in CSM mode). </span>

<span style="background: #ffffcc">This is important to boot the MX
installer on a UEFI system with CSM because the MX installer chooses
whether to install the BIOS bootloader or the native UEFI bootloader
depending on the way it is booted (UEFI or Legacy)</span>

<span style="background: #ffffcc">Another UEFI-related topic is the
so-called “secure boot” mechanism. Secure boot is a function of UEFI
implementations that allows the firmware to only load and execute code
that is cryptographically signed with certain keys and thereby blocking
any (potentially malicious) boot code that is unsigned or signed with
unknown keys.</span>

<span style="background: #ffffcc"><span lang="en-US">The indication that
we give, valid for most users, is to disable Secure Boot by accessing
the BIOS or Uefi settings when the computer starts. Once the Secure Boot
is disabled, however, the correct procedure to </span><span
lang="en-US">booting</span><span lang="en-US"> the computer with UEFI
may vary depending on the manufacturer and whether the CSM function is
present or not.</span></span>

<span style="background: #ffffcc"><span lang="en-US">It may be enough to
simply d</span><span lang="en-US">isable</span><span lang="en-US"> the
"secure boot" while in other cases you must then activate the "legacy
support" function.</span></span>

***<span style="background: #ffffcc"><span lang="en-US">D</span><span
lang="en-US">isable</span><span lang="en-US"> "</span><span
lang="en-US">S</span><span lang="en-US">ecure </span><span
lang="en-US">B</span><span lang="en-US">oot" </span><span
lang="en-US">function</span></span>***

<span style="background: #ffffcc">On some systems, the option to disable
secure boot is only made visible when a BIOS password has been set by
the user, so if you have a system with secure boot enabled, but cannot
find an option to disable it, try setting a BIOS password, powercycle
the machine and look again for an appropriate option. </span>

#### <span style="background: #ffffcc">**The Black Screen** </span> {#the-black-screen .western}

<span style="background: #ffffcc">Occasionally it may happen that you
end up looking at an empty black screen that may have a blinking cursor
in the corner. This represents a failure to start X, the windows system
used by Linux, and is most often due to problems with the graphics
driver being used. </span><span
style="background: #ffffcc">Solution</span><span
style="background: #ffffcc">: reboot and select Safe Video or Failsafe
boot options in the menu (F6); details on these boot codes </span><span
style="background: #ffffcc">in the anti</span><span
style="background: #ffffcc">X</span><span
style="background: #ffffcc">-FAQ, section </span>[<span
style="background: #ffffcc">“Live Boot
Parameters”</span>](https://download.tuxfamily.org/antix/docs-antiX-19/FAQ/boot-params.html)<span
style="background: #ffffcc"> </span><span style="background: #ffffcc">or
</span><span style="background: #ffffcc">in </span><span
style="background: #ffffcc">the</span><span style="background: #ffffcc">
</span>[<span style="background: #ffffcc">MX/antiX
Wiki</span>](https://mxlinux.org/wiki/system/boot-parameters)<span
style="background: #ffffcc">. </span>

\

***<span style="background: #ffffcc">The boot menu does not call up the
newly installed system or any other operating systems</span>***

<span style="background: #ffffcc"><span lang="en-US">On some UEFI
systems with CSM the </span><span lang="en-US">default </span><span
lang="en-US">boot mode used for removable </span><span
lang="en-US">devices</span><span lang="en-US"> may be different from
what is actually used when booting from hard disk so when booting the
installer from a USB stick in a different mode from what is used when
booting another already installed operating system from the hard disk,
the wrong bootloader might be installed and the system might be
unbootable after finishing the installation. </span></span>

<span style="background: #ffffcc">Once the installation is complete, if
at the next reboot in the boot menu you don't have the items to boot any
other operating system installed on your computer, then you can use the
tool: Boot Repair located in the antiX Control Centre.</span>

<span style="background: #ffffcc">In addition, if you are unable to call
up any Windows system in the menu, you will sometimes need to open a
terminal and type:</span>

<span style="background: #ffffcc">update-grub</span>

***<span style="background: #ffffcc">Disabling the Windows “fast boot”
feature</span>***

span style="background: \#ffffcc"&gt;Windows 8 and later offer a feature
called “fast boot” to cut down system startup time. Technically, when
this feature is enabled, Windows does not do a real shutdown and a real
cold boot afterwards when ordered to shut down, but instead does
something resembling a partial suspend to disk to reduce the “boot”
time. As long as Windows is the only operating system on the machine,
this is unproblematic, but it can result in problems and data loss when
you have a dual boot setup in which another operating system accesses
the same filesystems as Windows does. In that case the real state of the
filesystem can be different from what Windows believes it to be after
the “boot” and this could cause filesystem corruption upon further write
accesses to the filesystem. Therefore in a dual boot setup, to avoid
filesystem corruption the “fast boot” feature has to be disabled within
Windows.

<span style="background: #ffffcc">It may also be necessary to disable
“fast boot” to even allow access to UEFI setup to choose to boot another
operating system or </span><span style="background: #ffffcc">the
</span><span style="background: #ffffcc">anti</span><span
style="background: #ffffcc">X</span>` boot loader`<span
style="background: #ffffcc">.</span><span style="background: #ffffcc">
On some UEFI systems, the firmware will reduce “boot” time by not
initialising the keyboard controller or USB hardware; in these cases, it
is necessary to boot into Windows and disable this feature to allow for
a change of boot order. </span>

<span style="background: #ffffcc">For </span><span
style="background: #ffffcc">troubleshooting</span><span
style="background: #ffffcc">, please consult the [MX/antiX
Wiki](https://mxlinux.org/wiki/system/uefi), </span><span
style="background: #ffffcc">or ask on the Forum</span><span
style="background: #ffffcc">. </span>

### <span style="background: #ffffcc"> The standard opening screen </span> {#the-standard-opening-screen .western}

``` {.western}
```

*<span style="background: #ffffcc">**Figure: LiveMedium boot screen of
x64 ISO** </span>*

<span style="background: #ffffcc">When the LiveMedium boots up, you will
be presented with a screen similar to the Figure above; the
</span>*<span style="background: #ffffcc">installed</span>*<span
style="background: #ffffcc"> screen looks quite different. Custom
entries may also appear in the main menu. Detailed Help on this screen
can be found in [this
document](https://download.tuxfamily.org/antix/docs-antiX-19/Boot_Menu/antiX-gfxboot.html).
</span>

#### []()<span style="background: #ffffcc">Main Menu entries </span> {#main-menu-entries .western}

<span style="background: #ffffcc">**Table 1: Menu entries in Live boot**
</span>

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  <span style="background: #ffffcc">Entry </span>                                                                                                                                                                                       <span style="background: #ffffcc">Comment </span>
  <span style="background: #ffffcc"><span>**antiX-19** (&lt;RELEASE DATE&gt;) </span></span>                                                                                                                                            <span style="background: #ffffcc"><span>This entry is selected by default, and is the standard way that most users will boot the Live system. Simply press Return to boot the system. </span></span>
  <span>*<span style="font-style: normal">**<span style="background: #ffffcc">Safe Video Mode</span>**</span>*</span>                                                                                                                   <span><span style="background: #c2e0ae">Disable KMS (kernel mode set) video drivers and force the use of the </span>*<span style="background: #c2e0ae">vesa</span>*<span style="background: #c2e0ae"> video driver. Try this option of the system seems to boot but the screen is blank. </span></span>
  **<span style="background: #ffffcc"><span>Virtual Box Video</span></span>**                                                                                                                                                           <span style="background: #ffffcc"><span>If, when starting the Live drive in Virtualbox, the default video doesn't work, try this option</span></span>
  <span>*<span style="font-style: normal">**<span style="background: #ffffcc">Failsafe Boot</span>**</span>*</span>                                                                                                                     <span style="background: #ffffcc"><span>In addition to forcing safe video, also load all drivers early in the boot process. Try this option if the system does not boot at all.</span></span>
  <span style="background: #ffffcc"><span>**Boot from Hard Disk** </span></span>                                                                                                                                                        <span style="background: #ffffcc"><span>Allows for user to select a stored ISO to boot. </span></span>
  **<span style="background: #ffffcc"><span>Memory Test </span></span>**                                                                                                                                                                <span style="background: #ffffcc"><span>Runs a test to check RAM. If this test passes then there may still be a hardware problem or even a problem with RAM but if the test fails then you know something is wrong.</span></span>
  **<span style="background: #ffffcc"><span>Switch to Grub Bootloader</span></span>**<span style="font-weight: normal"><span style="background: #ffffcc"><span> option</span><span style="font-weight: normal">s</span></span></span>   <span style="background: #ffffcc"><span>It allows you to view some advanced options and recovery options of the bootloader, if it has problems.</span></span>
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

<span style="background: #ffffcc">In the bottom row the screen displays
a number of vertical entries, below which is a row of horizontal
options; **press F1 when looking at that screen for details**. </span>

#### []()<span style="background: #ffffcc">**Options** </span> {#options .western}

-   <span style="background: #ffffcc">**F2 Language**. Set the language
    for the bootloader and the antiX system. This will automatically
    transfer to the hard drive when you install. </span>
-   <span style="background: #ffffcc">**F3 Time Zone**. Set the timezone
    for the system. This will automatically transfer to the hard drive
    when you install. </span>
-   <span style="background: #ffffcc">**F4 Options**. Options for
    checking and booting the Live system. Most of these options do not
    transfer to the hard drive when you install. </span>
-   <span style="background: #ffffcc">**F5 Persist**. Options for
    retaining changes to the LiveUSB when the machine shuts down.
    </span>
-   **<span style="background: #ffffcc">F6 Desktop. </span>**<span
    style="font-weight: normal"><span style="background: #ffffcc">AntiX
    offers IceWM as the default installed Window Manager along with Rox
    File Manager, but the system has several other WM and FM installed.
    Here you can choose which combination you want to make
    the default.</span></span>
-   <span style="background: #ffffcc">**F7 Console**. Set resolution of
    virtual consoles. May conflict with Kernel Mode Setting. Can be
    useful if you are booting into Command Line Install or if you are
    trying to debug the early boot process. This option will transfer
    when you install. </span>
-   []() **<span style="background: #ffffcc">F</span><span
    style="background: #ffffcc">8</span><span
    style="background: #ffffcc"> </span><span
    style="background: #ffffcc">Save Bootloader Settings. </span>**<span
    style="font-weight: normal"><span style="background: #ffffcc">On
    LiveUSBs and Frugal installs, the </span></span>*<span
    style="font-weight: normal"><span style="background: #ffffcc">F8
    Save</span></span>*<span style="font-weight: normal"><span
    style="background: #ffffcc"> menu should appear. A LiveUSB made with
    the "dd" command acts like a LiveCD and does not have the
    </span></span>*<span style="font-weight: normal"><span
    style="background: #ffffcc">F8 </span></span><span
    style="background: #ffffcc">Save</span>*<span
    style="background: #ffffcc"> menu. </span>

    <span style="background: #ffffcc">S</span>*<span
    style="background: #ffffcc">ave.</span>*<span
    style="background: #ffffcc"> </span><span
    style="background: #ffffcc">Save </span><span
    style="background: #ffffcc">the current function key popup menu
    settings as the defaults and create/replace a custom main menu entry
    if needed for options that are typed if they don't exist in the
    popup menus. </span>

    *<span style="background: #ffffcc">Reset </span>*<span
    style="background: #ffffcc">Restore the original menu defaults. Does
    not affect the custom main menu entry (if one was created)</span>

\

<span style="background: #ffffcc">Other cheat codes for LiveUSB can be
found in</span><span style="background: #ffffcc"> the antix-FAQ, section
</span>[<span style="background: #ffffcc">“Live Boot
Parameters”</span>](https://download.tuxfamily.org/antix/docs-antiX-19/FAQ/boot-params.html)<span
style="background: #ffffcc"> </span><span style="background: #ffffcc">or
</span><span style="background: #ffffcc">in </span><span
style="background: #ffffcc">the</span><span style="background: #ffffcc">
</span>[<span style="background: #ffffcc">MX/antiX
Wiki</span>](https://mxlinux.org/wiki/system/boot-parameters)<span
style="background: #ffffcc">. </span><span
style="background: #ffffcc">The cheat codes for booting an installed
system are different </span><span style="background: #ffffcc">compared
to a Live system</span><span style="background: #ffffcc">, and can be
found in the same location. </span>

<span style="background: #ffffcc">MORE: </span>

[<span
style="background: #ffffcc">antiX19-Boot-Menu</span>](https://download.tuxfamily.org/antix/docs-antiX-19/Boot_Menu/antiX-gfxboot.html)

<span style="background: #ffffcc">[Linux startup
process](http://en.wikipedia.org/wiki/Linux_startup_process) </span>

### <span style="background: #ffffcc"> The UEFI opening screen </span> {#the-uefi-opening-screen .western}

<span
style="background: #ffffcc">![](FaqAntiXHomeEngColor_files/FaqAntiXHomeEngColor_htm_ca319a1592ead09.jpg){width="640"
height="356"} </span>

<span style="background: #ffffcc">***LiveMedium boot screen of x64 when
UEFI detected*** </span>

<span style="background: #ffffcc">If the user is using a computer set
for UEFI boot (see: "Boot the LiveMedium" paragraph), the opening screen
for UEFI Live boot will appear instead with different choices. </span>

-   <span style="background: #ffffcc">antiX-19\_x64 </span>
-   <span style="background: #ffffcc">Customize boot (with menus)</span>
-   <span style="background: #ffffcc">Advanced Options</span>
-   <span style="background: #ffffcc">Memory Test</span>
-   <span style="background: #ffffcc">Boot Rescue Menus</span>

<span style="background: transparent">**<span
style="background: #ffffcc">If you want </span>****<span
style="background: #ffffcc">localization</span>****<span
style="background: #ffffcc"> </span>**<span
style="background: #ffffcc">(recommended for non-English speaking users)
</span>**<span style="background: #ffffcc">or other options, choose
</span>****<span style="background: #ffffcc">"Custom</span>******<span
style="background: #ffffcc">ize</span>******<span
style="background: #ffffcc"> boot."</span>****<span
style="background: #ffffcc"> That will bring up as second screen of
extensive menu options; just select what you want and follow the
prompts. </span>**</span>

### *<span style="font-style: normal">AntiX </span><span style="font-style: normal">Live </span><span style="font-style: normal">default </span><span style="font-style: normal">desktop</span>* {#antix-live-default-desktop .western}

*![](FaqAntiXHomeEngColor_files/FaqAntiXHomeEngColor_htm_c9fb6ce6512ed865.png){width="481"
height="359"}*

<span style="background: #ffffcc">****The antiX,**** ****Live mode,****
****d******esktop****</span>

\
\

<span style="background: transparent">*<span
style="font-style: normal"><span style="background: #ffffcc">antiX opens
a default desktop consisting of IceWM as Wndows Manager associated with
Rox as File Manager.</span></span>*</span>

<span style="background: transparent">*<span
style="font-style: normal"><span
style="background: #ffffcc">T</span></span><span
style="font-style: normal"><span style="background: #ffffcc">here are
several other associations </span></span><span
style="font-style: normal"><span style="background: #ffffcc">between
Windows Manager and File manager</span></span><span
style="font-style: normal"><span style="background: #ffffcc">. Each user
can choose which association h</span></span><span
style="font-style: normal"><span
style="background: #ffffcc">e</span></span><span
style="font-style: normal"><span style="background: #ffffcc">
prefer</span></span><span style="font-style: normal"><span
style="background: #ffffcc">s</span></span><span
style="font-style: normal"><span style="background: #ffffcc"> and set it
as the default for h</span></span><span style="font-style: normal"><span
style="background: #ffffcc">is</span></span><span
style="font-style: normal"><span style="background: #ffffcc">
desktop.</span></span>*</span>

<span style="background: transparent">*<span
style="font-style: normal"><span style="background: #ffffcc">The various
possibilities are visible by going to Menu-start →Desktop
→</span></span><span style="font-style: normal"><span
style="background: #ffffcc">Other</span></span><span
style="font-style: normal"><span style="background: #ffffcc">
Desktop</span></span><span style="font-style: normal"><span
style="background: #ffffcc">s</span></span><span
style="font-style: normal"><span
style="background: #ffffcc">.</span></span>*</span>

<span style="background: transparent">*<span
style="font-style: normal"><span style="background: #ffffcc">By choosing
one of the presented combinations, the user can instantly see what one
of the alternative desktops, to the default one, looks like.
</span></span><span style="font-style: normal"><span
style="background: #ffffcc">Y</span></span><span
style="font-style: normal"><span style="background: #ffffcc">ou do not
need to exit the desktop </span></span><span lang="en-US"><span
style="font-style: normal"><span style="background: #ffffcc">and redo
the Login</span></span></span><span style="font-style: normal"><span
style="background: #ffffcc">.</span></span>*</span>

<span style="background: transparent">*<span
style="font-style: normal"><span style="background: #ffffcc">We will
review the various combinations in the Post-Installation
</span></span><span style="font-style: normal"><span
style="background: #ffffcc">paragraph</span></span><span
style="font-style: normal"><span style="background: #ffffcc">.
</span></span>*</span>

<span style="background: transparent">*<span
style="font-style: normal"><span style="background: #ffffcc">In any
case, it is not necessary to choose, before installation, the type of
Windows/File Manager association you prefer. Even after installation the
choice can be made, and changed, at any time.</span></span>*</span>

*<span style="font-style: normal"><span
style="background: #ffffcc">Thanks to the "</span></span><span
style="font-style: normal"><span
style="background: #ffffcc">Other</span></span><span
style="font-style: normal"><span style="background: #ffffcc">
Desktop</span></span><span style="font-style: normal"><span
style="background: #ffffcc">s</span></span><span
style="font-style: normal"><span style="background: #ffffcc">" option,
you</span></span><span lang="en-US"><span
style="font-style: normal"><span style="background: #ffffcc"> can choose
the Desktop you prefer, and then when you log out, the system will
remember your choice and </span></span></span><span lang="en-US"><span
style="font-style: normal"><span style="background: #ffffcc">then it
</span></span></span><span lang="en-US"><span
style="font-style: normal"><span style="background: #ffffcc">will
re-appear with the same Desktop that you had before leaving the
session.</span></span></span>*

<span style="background: #ffffcc">On the desktop of a Live system <span
lang="en-US">(with the exception of your Live Snapshot)</span> there
will be an "Installer" icon. Clicking on it will start the system
installation process.</span>

<span style="background: transparent">*<span lang="en-US"><span
style="font-style: normal"><span style="background: #ffffcc">On an
installed system this icon will no longer be
present.</span></span></span>*</span>

\

<span style="background: transparent">*<span
style="font-style: normal"><span style="background: #ffffcc">Before
installation, it can be useful to open the </span></span><span
style="font-style: normal"><span
style="background: #ffffcc">a</span></span><span
style="font-style: normal"><span style="background: #ffffcc">ntiX
Control Center, which is very rich in system administration tools. In
particular, it can help in </span></span><span lang="en-US"><span
style="font-style: normal"><span
style="background: #ffffcc">checking</span></span></span><span
style="font-style: normal"><span style="background: #ffffcc"> the
recognition by the system of the various peripherals. Keep in mind that,
in some cases, some devices, such as the WiFi card, will only work after
installation.</span></span>*</span>

\
\

![](FaqAntiXHomeEngColor_files/FaqAntiXHomeEngColor_htm_d4aa9174905ed4ff.png){width="710"
height="368"} *<span lang="en-US">***<span
style="background: #ffffcc">antiX menu shows several alternative
desktops, on the right: the main window of the antiX Control
Center</span>***</span>*

### <span>Can I install applications when running </span>*<span>live</span>*<span>?</span> {#can-i-install-applications-when-running-live .western}

<span style="background: transparent">*<span lang="en-US">*<span>Yes you
can and if you decide to install during that </span>*</span><span
lang="en-US"><span>live</span></span><span lang="en-US">*<span> session,
they will carry over to installation.</span>*</span>*</span>

### <span>Will the language, keyboard settings I chose at boot menu carry over to install?</span> {#will-the-language-keyboard-settings-i-chose-at-boot-menu-carry-over-to-install .western}

<span>Yes, in fact this is the best way to install your
</span>*<span>localised</span>*<span> antiX.</span>

<span lang="en-US"><span style="background: #ffffcc">In a GRUB system,
on boot </span></span><span lang="en-US"><span
style="background: #ffffcc">screen</span></span><span lang="en-US"><span
style="background: #ffffcc">,</span></span><span lang="en-US"><span
style="background: transparent"> </span></span><span
lang="en-US"><span>s</span></span><span>et your options via the
</span>*<span>F2</span>*<span> and </span>*<span>F3</span>*<span> keys
and if you use a writable device, use </span>*<span>F8</span>*<span> to
save your choices.</span>

<span style="background: #ffffcc">In a UEFI system, set "Customize Boot
with menus".</span>

<span style="background: transparent">*<span
lang="en-US">*<span>Alernatively, you can do this manually by setting a
variety of </span>*</span><span
lang="en-US"><span>cheats</span></span><span lang="en-US">*<span>. For
example: </span>*</span><span
lang="en-US"><span>lang=en</span></span><span lang="en-US">*<span>
</span>*</span><span lang="en-US"><span>kbd=gb,gr</span></span><span
lang="en-US">*<span> </span>*</span><span
lang="en-US"><span>tz=Europe/Athens</span></span><span
lang="en-US">*<span> will give a US English desktop, a toggled Greek,
British English keyboard and the timezone set to Athens,
Greece.</span>*</span>*</span>

<span style="background: transparent">*<span lang="en-US">*<span
style="background: #ffffcc">You may decide to enter these codes at boot
time if no other system works, but remember that the system and keyboard
localization can be very easy after opening the desktop using the
specific localization functions contained within the antiX Control
Center.</span>*</span>*</span>

### <span style="background: #ffffcc"> *The Installation process* </span> {#the-installation-process .western}

### *<span style="background: #ffffcc">Detailed installation steps </span>* {#detailed-installation-steps .western}

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  <span style="background: #ffffcc">![figura grafix / video\_camera\_rf.png](FaqAntiXHomeEngColor_files/FaqAntiXHomeEngColor_htm_7ea911ec27859df4.png){width="75" height="38"} </span> *video*: [<span style="background: #ffffcc">Installing antix17</span>](https://www.youtube.com/watch?v=uNf9-GRr_8M)   <span style="background: #ffffcc">![figura grafix / video\_camera\_rf.png](FaqAntiXHomeEngColor_files/FaqAntiXHomeEngColor_htm_7ea911ec27859df4.png){width="75" height="38"} </span> *video*: [<span style="background: #ffffcc">Installing antiX16</span>](https://www.youtube.com/watch?v=H_uoEdhr9dk&feature=youtu.be)   <span style="background: #ffffcc">![figura grafix / video\_camera\_rf.png](FaqAntiXHomeEngColor_files/FaqAntiXHomeEngColor_htm_8034d760c11ba3f4.png){width="75" height="38"} </span> *video*: [<span style="background: #ffffcc">Make a live from Windows</span>](https://www.youtube.com/watch?v=63ZR6qN2f8A)
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

\

<span style="background: #ffffcc">To begin, boot to the LiveMedium, then
click on the Installer icon in the upper left corner. If the icon is
missing (<span lang="en-US">e.g. on an ISO remastered with ISO
Snapshot</span><span lang="en-US">)</span>, click F4 and enter: *sudo
minstall* ** <span style="font-style: normal">(user </span><span
style="font-style: normal">and</span><span style="font-style: normal">
password, on LiveMedium: </span><span
style="font-style: normal">**demo**</span><span
style="font-style: normal">)</span>.</span>

<span
style="background: #ffffcc">![](FaqAntiXHomeEngColor_files/FaqAntiXHomeEngColor_htm_ac17199426952b62.jpg){width="400"
height="290"} </span>

<span style="background: #ffffcc">**Installer Screen** **Home** </span>

-   <span style="background: #ffffcc">The right side of the Installer
    screen presents user choices as the installation proceeds; the left
    side provides clarification of the content of the right side.</span>
-   <span style="background: #ffffcc">Keyboard Settings permits changing
    the keyboard for the installation process.</span>

<span
style="background: #ffffcc">![](FaqAntiXHomeEngColor_files/FaqAntiXHomeEngColor_htm_156d0d3ce23973f0.jpg){width="440"
height="314"} </span>

**<span style="background: #ffffcc">Installer set to Auto-install, with
Encryption activated</span>**

*<span style="background: #ffffcc">**Rearrange** **** **disk
partitions** **** **(optional)**</span>*

\

-   <span style="background: #ffffcc">**Modify partitions**. <span
    lang="en-US"><span style="font-style: normal">This option starts
    Gparted and allows you to modify existing partitions on the disk,
    however many users have concluded that if you need to, it is best to
    create or modify them before starting the installation process to
    avoid possible problems - </span></span><span lang="en-US"><span
    style="font-style: normal">for instance newly created partitions may
    not show up in the drop-down menus</span></span><span
    lang="en-US"><span style="font-style: normal"> under
    "Choose Partitions". So it is better to create them either
    </span></span><span lang="en-US"><span
    style="font-style: normal">on</span></span><span lang="en-US"><span
    style="font-style: normal"> </span></span><span lang="en-US"><span
    style="font-style: normal">MS</span></span><span lang="en-US"><span
    style="font-style: normal">Windows using specific applications for
    this task, or </span></span><span lang="en-US"><span
    style="font-style: normal">on a</span></span><span
    lang="en-US"><span style="font-style: normal">
    Cd/dvd/Usb</span></span><span lang="en-US"><span
    style="font-style: normal">-Live</span></span><span
    lang="en-US"><span style="font-style: normal"> using the
    Gparted application. Once the partitioning is complete, you should
    </span></span><span lang="en-US"><span
    style="font-style: normal">shutdown the</span></span><span
    lang="en-US"><span style="font-style: normal"> Live and
    re</span></span><span lang="en-US"><span
    style="font-style: normal">boot</span></span><span
    lang="en-US"><span style="font-style: normal"> it, and then
    </span></span><span lang="en-US"><span
    style="font-style: normal">proceed with</span></span><span
    lang="en-US"><span style="font-style: normal"> the installation.
    </span></span></span>

    *<span style="background: #ffffcc">Note. Clicking the "Run Partition
    Tool" button will start Gparted, as we said. It might be useful to
    open it even if you don't want to partition, just to see how the
    disk partitions are organized, if there was any doubt about it.
    Closing Gparted, this first installation screen that we are
    describing will reappear.</span>*

\

***<span style="background: #ffffcc">Select type of
installation</span>***

-   **<span style="background: #ffffcc">Use disk</span>**<span
    style="background: #ffffcc">. If unsure which is the partition you
    want, use the names you see in GParted. The disk you select will be
    examined cursorily for reliability by
    [SMART](https://en.wikipedia.org/wiki/S.M.A.R.T.). If problems are
    detected, you will see a warning screen. You will need to decide
    whether to accept that risk and continue, select another disk or
    terminate the installation. For more information, click
    </span>**<span style="background: #ffffcc">Start menu &gt;
    System &gt; GSmartControl </span>**<span
    style="background: #ffffcc">and “Perform tests” on the drive.
    </span>

<span style="background: #ffffcc">![figure
grafix/SMART.jpg](FaqAntiXHomeEngColor_files/FaqAntiXHomeEngColor_htm_96056c0c5d591670.jpg){width="249"
height="112"} </span>

<span style="background: #ffffcc">**SMART warning of risk of failure**
</span>

-   <span style="background: #ffffcc">**Auto-install using entire
    disk**. Select this option if you plan to use the entire hard drive
    for antiX and you aren't particular about how the partitions are
    set up. You can optionally specify an amount of space to leave
    unused, if you plan to create more partitions afterward. </span>

    <span style="background: #ffffcc"><span lang="en-US">**Make sure you
    understand that selecting this option will delete** </span><span
    lang="en-US">**all existing partitions**</span><span lang="en-US">
    **and data**</span><span lang="en-US">. </span>Only choose this if
    you are not going to keep anything on the selected hard drive.
    </span>

    -   <span style="background: #ffffcc">A pop-up message asks you to
        confirm using the entire disk.</span>

*<span style="background: #ffffcc">Note. By default, the installer
chooses "Auto-install using entire disk" if it detects only one
partition, but if it detects multiple partitions, it will be presented
as the default choice "Custom install on existing partitions" allowing
you to choose any other partition you wish to install on.</span>*

\

-   <span style="background: #ffffcc">**Encrypt**<span
    style="font-weight: normal">. Full</span> disk encryption is
    available for the first time with antiX-19.</span>

    <span style="background: #ffffcc">You can fine-tune your encryption
    cipher settings with the "Advanced Encryption Settings” button or
    just keep the defaults.</span>

<span
style="background: #ffffcc">![](FaqAntiXHomeEngColor_files/FaqAntiXHomeEngColor_htm_e0bed89bd66f36b.jpg){width="480"
height="314"} </span>

**<span style="background: #ffffcc">Installer set to “Custom install on
existing partitions”</span>**

\

-   <span style="background: #ffffcc">**Custom install on
    existing partitions.** <span style="font-style: normal"><span
    style="font-weight: normal">If you are installing
    </span></span><span style="font-style: normal"><span
    style="font-weight: normal">anti</span></span><span
    style="font-style: normal"><span style="font-weight: normal">X Linux
    to a dual-boot with another operating system, or you wish to define
    the sizes of your partitions manually, you need to select
    this option. If you have not previously set up your partitions,you
    may click “</span></span><span style="font-style: normal"><span
    style="font-weight: normal">Run partition tool” </span></span><span
    style="font-style: normal"><span
    style="font-weight: normal">button</span></span><span
    style="font-style: normal"><span style="font-weight: normal">
    </span></span><span style="font-style: normal"><span
    style="font-weight: normal">to run Gparted at this point and
    create them.</span></span></span>

    <span style="background: #ffffcc">Obviously, the above mentioned
    contra-indications must be considered, however in most cases there
    are no problems and eventually you can stop the installation, exit
    Live, reboot and try again with the modified partitions. </span>

    <span style="background: #ffffcc">However, whether you created the
    partitions clicking “Run partition tool” button or <span
    lang="en-US">launching </span>Gparted (or other suitable tools)
    before starting the installer, in any case select this option and
    click the "Next" button only after you have prepared the partitions
    you will need in the next step. </span>

#### <span style="background: #ffffcc">![](FaqAntiXHomeEngColor_files/FaqAntiXHomeEngColor_htm_a99e0769b8725f1e.jpg){width="480" height="314"} </span> {#section .western}

**<span style="background: #ffffcc">Installer looking for partition
choice</span>**

***<span style="background: #ffffcc">Choose partitions</span>***

\

<span style="background: #ffffcc">(If you chose </span>**<span
style="background: #ffffcc">Auto-install using entire disk</span>**<span
style="background: #ffffcc"> </span><span lang="en-US">in the previous
steps</span><span style="background: #ffffcc">, you will not see this
screen.) </span>

-   <span style="background: #ffffcc">Specify the root and swap
    partitions you want to use. If you set up a separate partition for
    your home directory, specify it here, otherwise leave /home set
    to root.</span>

    -   <span style="background: #ffffcc">Note that the user's /home
        folder will be inside the same (root) partition where antiX is
        being installed. </span>
    -   <span style="background: #ffffcc">Many users prefer to locate
        their home directory in a different partition that that of /
        (root), so that any problem with — or even total replacement of
        — the installation partition will leave all the user's
        individual settings untouched. </span>
-   <span style="background: #ffffcc">You can change the label of the
    partition where you want to install (e.g., to “antiX-19
    Testing Installation”)</span>
-   <span style="background: #ffffcc">Finally, you can optionally select
    the type of file system you want to use on the hard drive. The
    default ext4 is recommended in antiX if you have no
    particular choice. </span>
-   <span style="background: #ffffcc">Encryption. The various partitions
    can be encrypted.</span>
-   <span style="background: #ffffcc">Unless you are using encrpytion or
    know what you are doing, leave boot set to root.</span>

***<span style="background: #ffffcc">Preferences</span>***

-   <span style="background: #ffffcc">Check Preserve data in /home if
    you are doing an upgrade and already have data in an existing
    home partition. This option is not generally recommended because of
    the risk that old configurations will not match the new
    installation, but can be useful in specific situations, e.g.
    repairing an installation. </span>
-   <span style="background: #ffffcc">Select Check for bad blocks if you
    want to do a scan for physical defects on the hard drive
    during formatting. This is recommended for users with older drives.
    </span>

<span
style="background: #ffffcc">![](FaqAntiXHomeEngColor_files/FaqAntiXHomeEngColor_htm_569421d62e1d60a5.jpg){width="480"
height="343"} </span>

\

**<span style="background: #ffffcc">Installer asking about boot
method</span>**

***<span style="background: #ffffcc">Select boot method</span>***

-   <span style="background: #ffffcc">While the main linux OS is being
    copied to hard drive, you will be asked about some additional
    configuration information. Figure shows the GRUB bootloader
    installation options.</span>
-   <span style="background: #ffffcc">Most average users will accept the
    default settings on this screen, which will install the bootloader
    into the very beginning of the disk. This is the usual location and
    will cause no harm.</span>
-   <span style="background: #ffffcc">If you install antiX as the only
    system on your hard disk, or in dual boot with Windows, or together
    with several other operating systems, you should install grub in the
    MBR or ESP (for UEFI system)</span>

    <span style="background: #ffffcc">These are the default choices. The
    installer will automatically select MBR or ESP depending on whether
    UEFI is present or not.</span>

    <span style="background: #ffffcc"><span lang="en-US">Instead,
    </span><span lang="en-US">if you want to keep the bootloader of
    another distribution already present </span><span lang="en-US">, be
    it grub-legacy, grub2 or anything else, then you can decide to
    install grub on the root partition </span><span
    lang="en-US">(PBR)</span><span lang="en-US"> or not install it
    at all. In this case, after installation, </span><span
    lang="en-US">you will need to make changes to the existing
    grub</span><span lang="en-US">. This choice is for
    advanced users.</span> </span>

-   <span style="background: #ffffcc">When you click Next, a pop-up
    message will check to see that you accept the location of the
    bootloader GRUB. Installing GRUB can take a few minutes in
    some situations. </span>
-   <span style="background: #ffffcc">Note that the partition
    shown (sda) is just an example; your particular selection of
    partition may well differ.</span>

<span
style="background: #ffffcc">![](FaqAntiXHomeEngColor_files/FaqAntiXHomeEngColor_htm_3e932f4ede4bafdf.jpg){width="453"
height="325"} </span>

**<span style="background: #ffffcc">Computer Network Names
Setup</span>**

***<span style="background: #ffffcc">Computer Network Names </span>***

-   <span style="background: #ffffcc">Many users choose a unique name
    for their computer: laptop1, MyBox, StudyDesktop, UTRA, etc. You may
    also just leave the default name as it is. </span>
-   <span style="background: #ffffcc">You can just click Next here if
    you have no computer network. </span>
-   <span style="background: #ffffcc">If you are not going to
    *host*<span style="font-style: normal"> shared network folders on
    your PC, then you can disable samba. This will not affect your PC's
    ability to access shares hosted elsewhere on
    your network.</span></span>

\
\

<span
style="background: #ffffcc">![](FaqAntiXHomeEngColor_files/FaqAntiXHomeEngColor_htm_4fc35244b69086da.jpg){width="400"
height="288"} </span>

***<span style="background: #ffffcc">Locale, Timezone, and Service
Settings</span>***

<span style="background: #ffffcc">***L****ocalization Defaults***</span>

-   <span style="background: #ffffcc">The default settings will usually
    be correct here, as long as you were careful to enter any exceptions
    at the LiveMedium boot screen. </span>

    <span style="background: #ffffcc">If the boot is done via GRUB, the
    localization will be chosen by pressing the F2 key, if it is done
    via UEFI you will have to choose the item: "Customize Boot
    with menus".</span>

-   <span style="background: #ffffcc">If you are not english speaking,
    then the correct localization is important, the other settings:
    clock, timezone, keyboard, can also be changed later, when the
    Window Manager is started, using the antiX Control Centre. You can
    also change the localization later (using the command:
    dpkg-reconfigure locales), but having already defined it before
    starting the installer will allow you to display the choice options
    and help dialogues in your language with less risk
    of confusion.</span>

<span
style="background: #ffffcc">![](FaqAntiXHomeEngColor_files/FaqAntiXHomeEngColor_htm_6cbb90a1bc1d24f5.jpg){width="520"
height="372"} </span>

<span style="background: #ffffcc">***Figure:*** ***Enable/Disable
Services***</span>

***<span style="background: #ffffcc">Common services to Enable</span>***

\

-   <span style="background: #ffffcc">This screen only shows if “View”
    was clicked on the Locale, Timezone & Services
    Setting screen.</span>
-   <span style="background: #ffffcc">Services are applications and
    functions associated with the kernel that provide capabilities for
    upper-level processes. If you are not familiar with a service, you
    should leave it alone. </span>
-   <span style="background: #ffffcc">These applications and functions
    require time and memory, so if you are concerned about the capacity
    of your computer, you can look at this list for items that you are
    sure you do not need. </span>
-   <span style="background: #ffffcc">If you later want to change or
    adjust the startup services you have some choices.</span>

    -   <span style="background: #ffffcc">a command-line tool called
        **sysv-rc-conf** is installed by default and must be run
        as root. </span>
    -   <span style="background: #ffffcc">A grafic tool in the <span
        style="font-weight: normal">Co</span><span
        style="font-weight: normal">ntrol </span><span
        style="font-weight: normal">C</span><span
        style="font-weight: normal">entre</span>: **Choose Startup
        Services**</span>
    -   <span style="background: #ffffcc">a graphical tool called
        **Boot-Up Manager (BUM)** can be installed from the repos.
        </span>

<span
style="background: #ffffcc">![](FaqAntiXHomeEngColor_files/FaqAntiXHomeEngColor_htm_890577b3f1fdd93c.jpg){width="429"
height="307"} </span>

***<span style="background: #ffffcc">User Configuration</span>***

***<span style="background: #ffffcc">Account</span>***

\

-   <span style="background: #ffffcc">The level of security on the
    passwords you choose here will depend greatly on the setting of the
    actual computer. A home desktop is generally less likely to be
    broken into. </span>
-   <span style="background: #ffffcc">If you check Autologin, you will
    be able to bypass the login screen and speed up the boot process.
    The downside of that choice is that anyone with some kind of access
    to your computer would be able to log directly into your account.
    You can later change your autologin preferences using the antiX
    Control Centre, “Options” tab, “Set Auto-Login” button. </span>
-   <span style="background: #ffffcc">You can transfer any changes you
    make to your Live desktop to the HD installation by checking the
    last box. A small amount of critical information (e.g., the name of
    your wireless Access Point) will be translated automatically.
    </span>

<span
style="background: #ffffcc">![](FaqAntiXHomeEngColor_files/FaqAntiXHomeEngColor_htm_12cf2d9622c6006b.jpg){width="460"
height="328"} </span>

***<span style="background: #ffffcc">Installation Complete</span>***

#### <span style="background: #ffffcc">**Comments** ****** </span> {#comments .western}

-   []()<span style="background: #ffffcc">After the system copy is
    finished and the configuration steps are complete, an "Installation
    Complete” screen will be presented and you are ready to go!</span>
-   <span style="background: #ffffcc">If you don't want to reboot after
    finishing installation, uncheck the automatic reboot option before
    clicking Finish.</span>

### []()<span style="background: #ffffcc"> Troubleshooting </span> {#troubleshooting .western}

#### <span style="background: #ffffcc">No operating system found</span> {#no-operating-system-found .western}

<span style="background: #ffffcc">When rebooting after an installation,
it sometimes happens that your computer reports that no operating system
or bootable disc was found. It also may not show another installed OS
such as Windows. Usually, these problems mean that GRUB did not install
properly, but that is easy to correct. </span>

-   <span style="background: #ffffcc">If you can boot into at least one
    partition, open there a root terminal and run this command: </span>

    <span style="background: #ffffcc">*update-grub* </span>

-   <span style="background: #ffffcc">Otherwise, proceed with the “Boot
    Repair” tool:</span>

    -   <span style="background: #ffffcc">Boot to the LiveMedium.
        </span>
    -   <span style="background: #ffffcc">Launch **Control Centre**
        **&gt;** **Maintenance** <span
        style="font-weight: normal">tab</span> **&gt;** **Boot Repair**.
        </span>
    -   <span style="background: #ffffcc">Make sure that “Reinstall GRUB
        Bootloader” is selected, then click OK. </span>
    -   <span style="background: #ffffcc">If this still does not fix it,
        you may have a faulty hard drive. Usually, you will have seen a
        SMART warning screen about it when you began your installation.
        </span>

#### <span style="background: #ffffcc">Data or other partition not accessible</span> {#data-or-other-partition-not-accessible .western}

<span style="background: #ffffcc">Partitions and drives other than the
one designated as boot may not be booted or require root access after
installation. There are a couple of ways to change this. </span>

-   **<span style="background: #ffffcc">GUI</span>**<span
    style="background: #ffffcc">. Click </span><span
    style="background: #ffffcc">on Control Centre</span><span
    style="background: #ffffcc"> &gt; </span><span
    style="background: #ffffcc">Disks tab</span><span
    style="background: #ffffcc"> &gt; </span>**<span
    style="background: #ffffcc">Manage </span><span
    style="background: #ffffcc">Disk</span><span
    style="background: #ffffcc">s</span>**<span
    style="background: #ffffcc">. Check anything you want mounted at
    boot and save; when you reboot it should be mounted and you will
    have access in </span><span style="background: #ffffcc">your file
    manager</span><span style="background: #ffffcc">. See [HELP: Disk
    Manager](https://mxlinux.org/wiki/help-files/help-disk-manager)
    for details. </span>
-   <span style="background: #ffffcc">**CLI**. Open your file manager
    (Rox or SpaceFM) as root (SpaceFM: Menu-Bar &gt; File &gt; Root
    Window; Rox: open a terminal as root, and type rox) and navigate to
    the file /etc/fstab; click on it to open it in a text editor. Look
    for the line containing the partition or drive to which you want
    access (you may need to type *blkid* in a terminal to identify
    the UUID). Change it following this example for a
    data partition.</span>

<span style="background: #ffffcc">This entry will cause the partition to
be automatically mounted at boot time, and also allow you to mount it
and umount it as a normal user. This entry will also cause the file
system to be checked periodically at boot time. If you don't want it
mounted automatically at boot time then change the options field from
"*user*" to "*user,noauto*". </span>

-   <span style="background: #ffffcc">If you don't want it checked
    regularly then change the final "2" to a "0". Since you have an ext4
    filesystem it is suggested that you enable the automated checking.
    </span>
-   <span style="background: #ffffcc">If the item is mounted but not
    showing in Thunar, add an additional "*comment=x-gvfs-show*" to the
    line in your fstab file, which will force the mount to be visible.
    In the example above, the change would look like this:</span>

<!-- -->

<span style="background: #ffffcc">NOTE: neither of these procedures will
change Linux permissions, which are enforced on the folder and file
level. See Section 7.3. </span>

###  {#section-1 .western}

#### <span style="background: #ffffcc">Locking up</span> {#locking-up .western}

<span style="background: #ffffcc">If antiX Linux is locking up during
installation, it is usually due to a problem with faulty computer
hardware, or a bad DVD. If you have determined that the DVD is not the
problem, it may be due to faulty RAM, a faulty hard drive, or some other
piece of faulty or incompatible hardware. </span>

-   <span style="background: #ffffcc">Add one of the Boot Options using
    F4 at boot or consulting the</span><span lang="en-US"><span
    style="background: #ffffcc"> “Live Boot Parameters” chapter in this
    guide or the</span></span><span style="background: #ffffcc">
    [MX/antiX Wiki](https://mxlinux.org/wiki/system/boot-parameters).
    The most common problem arises from the graphic driver . </span>
-   <span style="background: #ffffcc">Your DVD drive may be
    having problems. If your system supports it, create an antiX
    bootable USB flash drive and install from that. </span>
-   <span style="background: #ffffcc">Systems often lock up due
    to overheating. Open the computer's case and ensure that all the
    system's fans are running when it is turned on. If your BIOS
    supports it, check the CPU and Motherboard temperatures (enter
    **sensors** in a root terminal if possible) and compare them to the
    temperature specifications for your system. </span>
-   <span style="background: #ffffcc">Shut down your computer and remove
    any non-essential hardware, then attempt the installation again.
    Non-essential hardware may include USB, serial, and parallel-port
    devices; removable PCI, AGP, PCIE, modem slot, or ISA expansion
    cards (excluding video, if you do not have onboard video); SCSI
    devices (unless you are installing to or from one); IDE or SATA
    devices that you are not installing to or from; joysticks, MIDI
    cables, audio cables, and any other external multimedia devices.
    </span>

\

\

*<span lang="en-US">*<span style="background: #ffffcc">P</span>*</span><span lang="en-US">*<span style="background: #ffffcc">ost</span>*</span><span lang="en-US">*<span style="background: #ffffcc">-installa</span>*</span><span lang="en-US">*<span style="background: #ffffcc">tion</span>*</span>* {#post-installation .western align="center"}
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

\

### <span>What window managers are available in antiX?</span> {#what-window-managers-are-available-in-antix .western}

<span>These window manager options come installed and ready to use in
antiX-full and antiX-base:</span>

-   <span>the lightweight Rox-IceWM (default) </span>
-   <span>the lightweight IceWM </span>
-   <span>the lightweight SpaceFM-IceWM </span>
-   <span>the minimalist manager Rox-Fluxbox </span>
-   <span>the minimalist manager Fluxbox </span>
-   <span>the minimalist manager SpaceFM-Fluxbox </span>
-   <span>the very minimalist manager Rox-JWM </span>
-   <span>the very minimalist manager JWM. </span>
-   <span>the very minimalist manager SpaceFM-JWM. </span>

<span>All window managers can be run with or without the ROX or SpaceFM
Desktop environment.</span>

<span><span style="background: #ffffcc">ROX </span><span
style="background: #ffffcc">and</span><span style="background: #ffffcc">
SpaceFM provide drag-and-drop functionality </span><span
style="background: #ffffcc">and allow the presence of icons </span><span
style="background: #ffffcc">or</span> the Conky system monitor that
displays real-time information.</span>

<span>antiX also comes with </span>*<span>herbstluftwm</span>*<span>, a
manual tiling window manager.</span>

### []()<span>What are the </span>*<span>min-</span>*<span> options?</span> {#what-are-the-min--options .western}

<span>If you want to keep RAM usage as low as possible, you can choose
one of the </span>*<span>min-</span>*<span> options. There is no
wallpaper, no desktop icons, no conky and no applications running in the
panel.</span>

### []() <span>How do I change from one WM to another?</span> {#how-do-i-change-from-one-wm-to-another .western}

<span><span style="background: #ffffcc">In IceWM and JWM</span> you can
switch by clicking on the menu → Desktop→ Other Desktops, <span
lang="en-US"><span style="background: #ffffcc">or for IceWM, Fluxbox and
JWM by right clicking anywhere on the desktop → Desktop →
</span></span><span lang="en-US"><span
style="background: #ffffcc">Other</span></span><span lang="en-US"><span
style="background: #ffffcc"> Desktop</span></span></span>

<span style="background: #ffffcc">It is possible to change the Window
Manager also at the login screen.</span>

<span style="background: #ffffcc"><span lang="en-US">The default desktop
of antiX-Full is IceWM-Rox, but in the login </span><span
lang="en-US">screen</span><span lang="en-US"> </span><span
lang="en-US">you</span><span lang="en-US"> </span><span lang="en-US">can
</span><span lang="en-US">switch from IceWM to </span><span
lang="en-US">an</span><span lang="en-US">other WM predisposed
</span><span lang="en-US">simply </span><span
lang="en-US">press</span><span lang="en-US">ing</span><span
lang="en-US"> the F1 key. </span></span>

### []()<span>And how to set the default one?</span> {#and-how-to-set-the-default-one .western}

<span style="display: inline-block; border: none; padding: 0in"><span
style="background: transparent">*<span lang="en-US"><span
style="font-style: normal"><span>Whichever one you have running before
reboot, will be the default.</span></span></span>*</span></span>

### <span>Where are my files?</span> {#where-are-my-files .western}

<span>As in all Linux versions, personal files are located in antiX by
default in the /home directory of the Linux file system.</span>

### <span>How are files managed?</span> {#how-are-files-managed .western}

<span>antiX has two graphical file managers, one of them is Rox-Filer
which can be opened clicking on the Home Folder icon in IceWM's taskbar
or from Menu -→ Home. Rox-Filer is a graphical file manager that is at
the heart of the ROX desktop (see ROX FAQ section). It opens to your
home directory, where you can change the view to include hidden system
files by clicking on the eye icon up on the menu bar. Right-click a file
to see management options. Among them is Options…, which manages ROX's
settings.</span>

<span>The other one is SpaceFM, found under Menu -→Applications -→
System Tools -→ SpaceFM. SpaceFM is a multi-panel tabbed file manager
for Linux with built-in VFS, udev-based device manager, customizable
menu system, and bash integration.</span>

<span>For a command-line file manager, click Menu -→ Terminal -→ and
type mc, which brings up Midnight Commander, whose main interface
consists of two panels displaying the file system with numbered commands
on the bottom. Press F1 for help.</span>

### <span>Can I synchronize files/folders?</span> {#can-i-synchronize-filesfolders .western}

<span>Go to antiX CC -→ Disks -→ Synchronize Directories or to Menu -→
Applications -→ System tools -→ Grsync.</span>

### <span>What application should I use for system setup?</span> {#what-application-should-i-use-for-system-setup .western}

<span>antiX comes with two applications to make system configuration or
managing users easier. They can be found in the antiX CC -→ System-→
Configure System or Manage Users.</span>

### <span>Are there any other antiX Assistants I should know about?</span> {#are-there-any-other-antix-assistants-i-should-know-about .western}

<span>The antiX user-management script unites a number of tools having
to do with user management. Found in antiX CC -→ System</span>

### <span>How do I install nvidia/ATI driver for antiX?</span> {#how-do-i-install-nvidiaati-driver-for-antix .western}

<span><span style="background: #ffffcc">About Nvidia,</span> t<span>he
best way to do this is via the included app available in the control
centre, </span>a<span>lternatively, use the smxi script. </span><span
style="background: #ffffcc">About ATI use the smxi script. See smxi
section.</span></span>

### []()<span>How to autologin?</span> {#how-to-autologin .western}

<span>The installer gives you the option to set auto-login. Control
Centre →Session →Set auto-login. You will need to reboot for changes to
take effect.</span>

### <span>What other system tools are provided?</span> {#what-other-system-tools-are-provided .western}

<span>Click Menu -→Applications -→ System Tools to get to System
Profiler and Benchmark, and Htop, an interactive process viewer.</span>

### []() <span>How to set the correct date and time?</span> {#how-to-set-the-correct-date-and-time .western}

<span>The best way is to do so at first boot when running live.
</span>*<span>F4</span>*<span> has these options.</span>

-   <span>hwclock=ask </span>
-   <span>hwclock=utc </span>
-   <span>hwclock=local </span>

<span>Default is set to UTC.</span>

<span>You are also given an option during installation to set the
hwclock.</span>

<span>If you missed the above, then there are 3 possible issues:</span>

<span>1) wrong timezone</span>

<span>2) wrong selection of UTC versus local time</span>

<span>3) BIOS clock set wrong</span>

<span>The first issue is addressed with </span>*<span>sudo
dpkg-reconfigure tzdata</span>*<span>. Do this first. You should also be
able to just check the current value with </span>*<span>cat
/etc/timezone</span>*<span>.</span>

<span>Once you are sure the timezone is correct, you can work on setting
your BIOS clock. Do this with the </span>*<span>hwclock</span>*<span>
command. First do a </span>*<span>man hwclock</span>*<span> and then run
</span>*<span>hwclock --show</span>*<span> to see what it is set to. It
always reports in localtime which is why you need to first make sure
your timezone is set correctly.</span>

<span>Use </span>*<span>hwclock --localtime</span>*<span> or
</span>*<span>hwclock --utc</span>*<span> depending on whether you want
your hardware clock to be set to localtime or utc. Most pure Linux
systems use utc. Most dual boot systems use localtime.</span>

<span>Then, after you get your date command working via the sudo command
you posted, you can use </span>*<span>hwclock --systohc</span>*<span> to
set the hardware clock so it matches your system time. Again, you need
the timezone and localtime/utc choice set correctly first (although if
you want to </span>**<span>assume</span>**<span> they are set correctly
already then this is the only command you need to run to get your
changes to the </span>*<span>date</span>*<span> command to stick. If you
assumed incorrectly then you will likely get mysteriously screwed by DST
a few times per year).</span>

<span>Finally, if you are having problems with hwclock drift or if you
are a perfectionist then you can install the
</span>*<span>ntp</span>*<span> package which will use time servers on
the net to keep your clock exactly on time. But you have to first go
through the steps above before </span>*<span>ntp</span>*<span> will work
correctly.</span>

### []()<span>How to edit sources list?</span> {#how-to-edit-sources-list .western}

<span>Either via synaptic (for </span>*<span>antiX-full</span>*<span>)
Control Centre→ System→ Manage Packages→ Settings→ Repositories or edit
individual files in /etc/apt/sources.d/ (for
</span>*<span>antiX-base</span>*<span> and
</span>*<span>antiX-core</span>*<span>)</span>

### []()<span>How to enable the Firewall?</span> {#how-to-enable-the-firewall .western}

*<span>Gufw</span>*<span> is installed, but not enabled. Open Control
Centre→ Network→ Manage Firewall</span>

### []() <span>How do I find which applications to install?</span> {#how-do-i-find-which-applications-to-install .western}

<span>Both </span>*<span>antiX-full</span>*<span> and
</span>*<span>antiX-base</span>*<span> come with
</span>*<span>package-installer</span>*<span>, which makes it easy to
install popular applications. See below for more information.</span>

*<span>antiX-full</span>*<span> also comes with
</span>*<span>synaptic</span>*<span> so searching for applications is
easy.</span>

<span>To search for applications in
</span>*<span>antiX-net</span>*<span>,
</span>*<span>antiX-core</span>*<span> and
</span>*<span>antiX-base</span>*<span> use our tool
</span>*<span>cli-aptiX</span>*<span> or use </span>*<span>apt-cache
search</span>*<span> in a terminal</span>

<span>For example: </span>*<span>apt-cache search video player</span>*

### []() <span>How do I keep the system up-to-date?</span> {#how-do-i-keep-the-system-up-to-date .western}

<span>antiX is set up using </span>*<span>Debian Stable</span>*<span>
repositories by default. This allows users to keep their system up to
date with regular upgrades. antiX recommends using </span>*<span>apt-get
update</span>*<span> followed by </span>*<span>apt-get
dist-upgrade</span>*<span> in a terminal. Synaptic is also available for
those that prefer a gui tool.</span>

### []() <span>DVD videos don't play. How come?</span> {#dvd-videos-dont-play.-how-come .western}

<span>You will need to install </span>*<span>libdvdcss2</span>*<span>
and maybe some codecs by enabling the
</span>*<span>deb-multimedia</span>*<span> repository (see above how to
do this) and then either search for
</span>*<span>libdvdcss2</span>*<span> in synaptic and then install or
use the command line.</span>

-   <span>apt-get update </span>
-   <span>apt-get install libdvdcss2 </span>

<span>The easiest way is to use the
</span>*<span>package-installer</span>*<span> application as it
automatically does the above for you.</span>

<span>antiX strongly advises users not to keep the deb-multimedia
repository enabled as there may be conflicts.</span>

### []() <span>Toggling Conky on/off:</span> {#toggling-conky-onoff .western style="line-height: 115%"}

<span style="background: #ffffcc">Right click on Desktop → Desktop→
Conky on/off</span>

### []()<span>How do I get out of antiX?</span> {#how-do-i-get-out-of-antix .western}

<span style="background: #ffffcc">Right click on the Desktop→Logout
there are several options including shutdown.</span>

### []() <span>I don't like that splash image. It takes up too much of my small screen space. How do I boot without it?</span> {#i-dont-like-that-splash-image.-it-takes-up-too-much-of-my-small-screen-space.-how-do-i-boot-without-it-1 .western}

<span>Easy. Just press </span>*<span>F7</span>*<span> console
(</span>*<span>F6</span>*<span> on core or net) and choose the
</span>*<span>default</span>*<span> option.</span>

### []() <span>Tell me more about <span style="background: #ffffcc">th</span><span style="background: #ffffcc">e</span> Desktop Settings</span> {#tell-me-more-about-the-desktop-settings .western}

<span>Since antiX is designed to work on older boxes, we have attempted
to make the desktop session more robust. Also, we have tried to make it
as easy as possible for new users and highlight its features. However,
depending on your hardware and tastes, you may wish/need to make some
changes. For example, increasing/decreasing the desktop startup delay.
The configuration files are found in Control Centre→Session→User Desktop
Session</span>

### []()<span>Can you give me an example?</span> {#can-you-give-me-an-example .western}

<span>Ok, I want to use IceWM without desktop icons, choose a colour
background, and only have the volume icon on the taskbar. My box is new,
so I want to reduce the startup delay and keep conky on the
desktop.</span>

<span>1.Boot using IceWM option or change to it via Menu-→Desktop-→Other
Desktops</span>

<span>2.Control Centre-→Session-→User Desktop Session click on
</span>*<span>startup</span>*<span> tab and comment \# entries you do
not want to use</span>

<span>3.Control Centre-→Session-→User Desktop Session click on
</span>*<span>desktop-session.conf</span>*<span> and make the following
changes Startup delay "0" Notification dialog "false"</span>

<span>4.Use the Wallpaper application in Control Centre-→Desktop to set
</span>*<span>No wallpaper</span>*<span> and a background colour.</span>

<span>5.Logout and log back in to your customised desktop. They will be
saved on reboot.</span>

### []()<span>Does flash work?</span> {#does-flash-work .western}

<span>If you still need to use flash, use the package-installer to do
so.</span>

### <span>How do I set up a wireless connection?</span> {#how-do-i-set-up-a-wireless-connection .western}

<span>There are 4 options, all found in the antiX Control Centre: <span
style="background: #ffffcc">Connman</span> (full only), wpa\_supplicant,
GRPS/UMTS and ceni. antiX developers recommend ceni.</span>

-   <span>In Control Centre -→ Network -→ you will find the wireless
    frontend <span style="background: #ffffcc">Connman</span> that antiX
    uses to make its wireless connection. </span>
-   <span>The command-line (CLI) tool Ceni from the aptosid developers
    is a wrapper that is very efficient for setting up a connection:
    Choose under Hardware interfaces either your wired or wireless
    connection, click on your choice and you can then configure that
    choice, it is an intuitive application. </span>
-   <span>For those with GRPS/UMTS use the application in the antiX CC.
    </span>
-   <span>Configure Dial-Up Connection using Gnome PPP in the antiXCC.
    </span>
-   <span>wpa\_gui is also included in the antiX Control Centre. </span>

### <span>What about dial-up?</span> {#what-about-dial-up .western}

<span>Control Centre -→ Network-→Configure Dial-Up Connection. This
brings up Gnome PPP, a graphical frontend for the excellent WvDial tool.
Intuitive interface makes it easy to use.</span>

### <span>How do I set up printing?</span> {#how-do-i-set-up-printing .western}

<span>Unless you have an HP printer (see next entry), do one of the
following:</span>

-   <span>In antiX CC -→ Hardware -→ Setup a Printer. A screen will open
    showing you the printers that have been found, with an icon to click
    for an assistant to add a new printer. </span>
-   <span>Open up a browser (Menu -→ Browser), and enter:
    <http://localhost:631/> This will take you to the CUPS interface,
    where you can set up your printer. For help on particular printers
    and drivers, check the OpenPrinting database. </span>

### <span>How do I set up an HP printer?</span> {#how-do-i-set-up-an-hp-printer .western}

<span>The easiest method is to use the Package-Installer, located in the
antiX CC -→System-→ Package-installer, then click on the arrow for
Office and tick the check box for HP\_printing, then Install.</span>

<span>If you prefer to do it yourself, then do the following, (Warning!
This may be incomplete due to subsequent changes in packages —use at
your own risk!)</span>

<span>You may need to install some additional packages:</span>

``` {.western}
hpijs
hpijs-ppd
hplip-data
hplip
hplip-gui
cups-pdf
cupsys-client
cupsys-driver-gutenprint
gs-gpl
lpr
magicfilter
gv
xprint-utils
```

<span>You should then be able to set up your printer by opening up a
browser (Menu -→ Browser), and entering: <http://localhost:631/></span>

### <span>What about a scanner?</span> {#what-about-a-scanner .western}

<span>This one should be easy. Connect your scanner to the computer, and
click Menu -→ Applications -→ Graphics -→ Simple Scan (xsane) or you can
dl via synaptic Xscanimage where, after installing and updating the
menu, launch Xscanimage, then selecting from the scanners it has found
(it may have multiple entries for your one scanner, so try them all
until one works), you will see a very basic screen which is pretty
self-evident. To adjust the values of your scanner, click Menu -→ Run
and type xcam.</span>

<span>You can then edit the scan in Mirage (Menu -→ Applications -→
Graphics). For details on using Mirage, check its User Manual.</span>

### <span>I want to configure my monitor - how do I do that?</span> {#i-want-to-configure-my-monitor---how-do-i-do-that .western}

<span>The easiest method is to use the Set Screen Resolution in antiX CC
-→ Session-→ Set Screen Resolution.</span>

<span>This will launch the application grandr.</span>

### <span>Can I use a MP3 player such as an iPod with antiX?</span> {#can-i-use-a-mp3-player-such-as-an-ipod-with-antix .western}

<span>You can play MP3 files with xmms, and you can view and edit ID3
tags by installing the program EasyTAG. But there is no simple way to
synchronize your player with your host computer without installing
Amarok and the KDE files it depends on.</span>

### <span>Is there a way to interact with a digital camera?</span> {#is-there-a-way-to-interact-with-a-digital-camera .western}

<span>The easiest method is to just treat it as a storage device by
using a USB card reader. Then when you connect it to the computer, it
will be mounted and available as an entry in the directory /media. You
can either click Menu -→ Files to examine and move or delete the
contents, or click Menu -→ Applications -→ Graphics and choose to bring
up Gtkam or Mirage to manipulate your images with the program of your
choice.</span>

### <span>Can I use a webcam?</span> {#can-i-use-a-webcam .western}

<span>Many web cams will work with antiX.</span>

<span>guvcview webcam application is located Applications-→Sound &
Video-→guvcview</span>

### <span>What if I need to edit a text file?</span> {#what-if-i-need-to-edit-a-text-file .western}

<span>You have five good options in antiX, from the lightest, to the
simplest, up to a full-featured one. Two are available by clicking Menu
-→ Tools -→ Editors.</span>

-   <span>Nano is a command-line application run by control keys listed
    at the bottom of the screen. Press Ctrl-G to access the help file.
    </span>
-   <span>To get to Leafpad simply click Menu -→ Editor . It's a simple
    graphical application that includes essential features and uses
    familiar pull-down menus. </span>
-   <span>Geany (Menu -→ Applications -→ Programming -→ Geany) is a
    small and fast editor with basic features of an integrated
    development environment like syntax highlighting etc. </span>
-   <span>LibreOffice (Menu -→Applications -→ Office -→
    LibreOffice Writer) LibreOffice is a power-packed free, libre and
    open source personal productivity suite for Windows, Macintosh
    and GNU/Linux. antiX comes with five of its feature-rich
    applications for all your document production and data processing
    needs: Writer, Calc, Impress, Draw and Math. </span>

<span>You can open any text for editing in the file managers Rox-filer,
SpaceFM and MC.</span>

### <span>What if a file is compressed in some format such as tar, etc?</span> {#what-if-a-file-is-compressed-in-some-format-such-as-tar-etc .western}

<span>Click Menu -→ Applications -→ Accessories-→ Archive Manager to
bring up File-Roller, migrate to the file location, highlight the file
and click Extract on the icon bar.</span>

### <span>How do I install and manage software?</span> {#how-do-i-install-and-manage-software .western}

<span>There are a number of methods of managing software in antiX, the
most common of which are the command-line application Apt and the
graphical user interface for it known as Synaptic.</span>

-   <span>To get to Apt just click Menu -→ Terminal and become root
    (type su followed by root's password). </span>
-   <span>To get to Synaptic, which is sometimes easier than Apt, open a
    root terminal as just described, then just type in the
    word synaptic. It is also found in the antiX CC -→ System, called
    Manage Packages. </span>
-   <span style="background: #ffffcc">Package Installer, even easier to
    use for a newbie than Synaptic. It facilitates the installation of
    popular packages. To start it: open the Control Center -&gt; System
    and click on the "Package Installer" button.</span>

### <span>How do I keep the system up-to-date?</span> {#how-do-i-keep-the-system-up-to-date-1 .western}

<span>Before either updating the system or installing software for the
first time, it's necessary to update the package database. Do this by
opening Synaptic (see above) and clicking on the Reload button or
opening a root terminal (Menu -→ Applications -→ Accessories -→ Root
Terminal) and type: apt-get update</span>

<span>Do not use Synaptic for a system upgrade! To upgrade software you
will have to use the terminal, either with Apt, Aptitude (needs
installing) or smxi.</span>

<span>antiX is based on the Debian Stable repositories. So to update the
system (in a root terminal) type: apt-get update (followed by) apt-get
dist-upgrade</span>

<span>If you receive any kind of error messages during the update, check
antiX forums at
[https://www.antixforum.com](https://www.antixforum.com/) to see if
someone already posted a solution for it. If not, please don't be shy
and ask for help.</span>

<span>For an off-line overview of Apt usage and resources open the
terminal and type man apt. Feel free to explore its subsections: man
apt-get, man apt-cache, man sources.list etc.</span>

### <span>After installing new programs do they appear in the menu?</span> {#after-installing-new-programs-do-they-appear-in-the-menu .western}

-   <span>Yes. This is a new feature included originally in antiX-15.
    Menus are automatically updated. </span>

<span>How can I get my email?</span>

<span>Clicking Menu -→Applications-→ Internet -→Claws Mail You enter
setup by following the wizard that pops up the first time you use the
application and filling in the blanks with the information for your
email account(s).</span>

### <span>How can I get my email?</span> {#how-can-i-get-my-email .western}

<span>Clicking Menu -→Applications-→ Internet -→Claws Mail You enter
setup by following the wizard that pops up the first time you use the
application and filling in the blanks with the information for your
email account(s).</span>

### <span>What does antiX offer for web browsing?</span> {#what-does-antix-offer-for-web-browsing .western}

<span>The default browser is the full-featured Firefox-esr. Click Menu
-→Web Browser or the taskbar icon to launch it. It's compatible with
many Firefox extensions.</span>

<span>Three other lightweight browsers are also included under Menu -→
Applications -→ Internet. Dillo is a fast, minimalistic multi-platform
web browser that is highly secure, while the links2 browser is a
graphics and text mode web browser that is also very fast and safe and
has pull-down menus available by clicking on the top bar. Finally elinks
is a text mode browser.</span>

<span>There are many other browsers available for download. Just have a
look in the package-installer.</span>

<span>If you have low RAM (1GB and less), antiX users recommend one of
the following for a lighter and faster experience than that offerred by
the default browser Firefox-esr.</span>

-   <span>Palemoon </span>
-   <span>Seamonkey </span>
-   <span>Slimjet </span>

### <span>Can I chat with antiX?</span> {#can-i-chat-with-antix .western}

<span>There are two chat programs that come installed with antiX, the
first two are located under Menu -→ Applications -→ Internet -→</span>

-   <span>Hexchat - an IRC client based on XChat. </span>
-   <span>Finally, clicking Menu-→Terminal-→ and typing irssi -→ brings
    up a speedy and very capable command-line IRC client. Setup and use
    is not obvious, so be sure to check the Irssi documentation. </span>

### <span>What can I use to burn a CD/DVD?</span> {#what-can-i-use-to-burn-a-cddvd .western}

-   <span>For a GUI app: click Menu -→Applications-→ Sound & Video -→
    Xfburn </span>
-   <span>For a CLI app: Click Menu -→Terminal-→ and type cdw this will
    launch the CLI app cdw </span>

### <span>What do I use for a news (RSS) reader?</span> {#what-do-i-use-for-a-news-rss-reader .western}

<span>You have a few good options:</span>

-   <span>Install a desktop aggregator from the repositories (use
    Synaptic and search on RSS feed). </span>
-   <span>RSS/Atom Reader -→Menu-→Terminal-→ and type newsbeuter-→ a CLI
    app called newsbeuter. </span>

### <span>How do I play music?</span> {#how-do-i-play-music .western}

<span>To play an audio CD, pop it in your CD player, then (there is no
autoplay) click Menu -→Applications-→Sound and Video-→xmms. This brings
up xmms, a multimedia player for Unix systems. Click on the forward
button (second from left) to start the CD. It may be necessary to point
the player to the correct device, i.e. /media/cdrom. (note: This “ ../ ”
means “move up one directory level”.)</span>

<span>You can select a different source of the audio files to play in
xmms. To do this, right-click on the xmms player top bar, and from the
pop-up menu select Play File to select the audio file to play. To play
an audio mp3 or ogg vorbis file, simply left-click on it and xmms opens
automatically and starts playing the file.</span>

<span>There is also a mixer available by clicking Menu -→ Control
Centre-→Hardware Tab -→ Adjust Mixer. The application is AlsaMixer and
it is pretty evident how to use it.</span>

<span>If you want to use a CLI console music player for your music on
hard drive: Menu-→ Terminal-→and type mocp-→ will launch moc.</span>

### <span>What about playing music from an internet source?</span> {#what-about-playing-music-from-an-internet-source .western}

<span>If you want to be able to play an audio file from the Internet,
open xmms and select Play Location, then enter the Internet location
(URL) of the file.</span>

<span>Alternatively, you can use Menu -→ Applications-→ Sound & Video -→
Streamruner2, which offers hundreds of thousands of music resources in a
fast and clean common interface.</span>

### <span>Can I rip music with antiX?</span> {#can-i-rip-music-with-antix .western}

-   <span>For a GUI app use Menu-→ Applications-→ Sound & Video-→
    Asunder CD Ripper This application can save tracks from an Audio CD
    as WAV, MP3, OGG, FLAC, and/or Wavpack. </span>
-   <span>There is also a cli ripping app in antiX called abcde. </span>

<span>The movie DVD I put in did not work. How can I watch
movies?</span>

<span>There is no autoplay feature, so after you put in your DVD you can
play it with either gxine, or GNOME MPlayer. Menu -→ Applications -→
Sound & Video -→ GNOME MPlayer: navigate to File -→ Disc to start
playback.</span>

<span>To play a video file (e.g., avi, mpeg etc.), simply left-click on
it and GNOME MPlayer will open automatically and start playing the
file.</span>

### <span>How do I change the desktop wallpaper?</span> {#how-do-i-change-the-desktop-wallpaper .western}

<span>Via antiX CC -→ Desktop -→ Choose Wallpaper. Note you can set a
different wallpaper for each WM!</span>

### <span>Is there a screensaver?</span> {#is-there-a-screensaver .western}

<span>Yes, it defaults to a black screen, with or without locking.
Alternatively, use the lock option in Exit. If you want you can add
xscreensaver from synaptic, or of course the CLI.</span>

### <span>What applications are available for standard office use?</span> {#what-applications-are-available-for-standard-office-use .western}

<span>All office applications are found by clicking Menu -→
Applications-→Office. antiX comes with LibreOffice.</span>

<span>Other office apps include a gui PDF reader, a cli PDF reader and a
calculator.</span>

###  {#section-2 .western}

\

------------------------------------------------------------------------

\
[]()<span>Links</span> {#links .western align="center"}
----------------------

-   <span>\[[antiX Home](https://antixlinux.com/)\]. </span>
-   <span>\[[antiX forum](https://www.antixforum.com/)\]. </span>

<span>Users really should have a look at the absolutely wonderful videos
made by dev team member dolphin\_oracle.</span>

-   <span style="background: #fff9ae">\[[antiX
    19](https://www.youtube.com/watch?v=mUicofUy3Bk)\]</span>
-   <span style="background: #fff9ae">\[</span>[<span
    style="background: #fff9ae">An Installation And First Look At AntiX
    19.3</span>](https://www.youtube.com/watch?v=M_el3cwVHDw)<span
    style="background: #fff9ae">\]</span>
-   <span style="background: #fff9ae">\[</span>[<span
    style="background: #fff9ae">antix mx 19 live usb
    configuration</span>](https://www.youtube.com/watch?v=kGi9jd1qW8g)<span
    style="background: #fff9ae">\]</span>
-   <span style="background: #add58a">\[[antiX-17 - What's
    new](https://www.youtube.com/watch?v=jpdl1ES1zOg)\] </span>
-   <span style="background: #add58a">\[[antiX 16 - UEFI install
    (with partitioning)](https://www.youtube.com/watch?v=2JlaO6RyUt4)\]
    </span>
-   <span style="background: #add58a">\[[Introducing
    antiX-16](https://www.youtube.com/watch?v=G8Aw2zzBE-g&feature=youtu.be)\]
    </span>
-   <span style="background: #add58a">\[[What's new in
    antiX-16](https://www.youtube.com/watch?v=a9kaIoieEbk)\] </span>
-   <span style="background: #add58a">\[[Installing
    antiX-16](https://www.youtube.com/watch?v=H_uoEdhr9dk&feature=youtu.be)\]
    </span>
-   *<span lang="en-US"><span style="font-style: normal"><span
    style="background: #add58a">\[</span></span></span>[<span
    style="font-style: normal"><span style="background: #add58a">antiX
    Videos</span></span>](https://www.youtube.com/user/runwiththedolphin)<span
    lang="en-US"><span style="font-style: normal"><span
    style="background: #add58a">\] </span></span></span>*

<div>

</div>

</div>
