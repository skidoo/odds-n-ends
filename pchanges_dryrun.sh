#!/bin/bash
###     pchanges_dryrun.sh
### Utility script for antiX livesession "dynamic root persistence" users.
### Generates a textfile containing a list of changed files slated
### for addition into to the roofs during a persist-save operation.
###
### Useful for honing  /usr/local/share/excludes/persist-save-exclude.list
###
### When launched with commandline option -d
### the script will also output to terminal the verbose stat/details of
### each file, along with a tally
### total number of files, and total BYTES slated for sending to rootfs
###
### NOTE: The script attempts to call LEAFPAD editor to display the output file.
###       Edit the script to change to your preferred editor, or none

[ $EUID != "0" ] &&  printf "must run this utility as superuser\n" && exit 1;
source /usr/local/lib/antiX/antiX-common.sh     "$@"

check_persist_uuid() {
    local uuid_dev=$(blkid -U "$PERSIST_UUID" -c /dev/null)
    [ -n "$uuid_dev" ] && return 0

    [ -z "$uuid_dev" ] &&  error_box \
        "$(pf $"The persistence device with UUID %s was not found" "[n]$PERSIST_UUID[/]")" \
        "$(pf $"Please plug in the persistence device and try again")"

    vpf "Using device %s with UUID %s" "[n]$uuid_dev[/]" "[n]$PERSIST_UUID[/]"
    PERSIST_DEV=$uuid_dev
}

showitemdetails() {
  mytally=0
  items=0
  cp /tmp/p_changes.out.txt /tmp/p_changes_rawlist.txt
  sed -i -e 's/^[^\/].*$//g' -e 's/^.*[\/]$//g' -e '/^$/d' /tmp/p_changes_rawlist.txt

  while LANG=C IFS= read -r inna ; do
      moo="$(stat "$inna" 2>&1 | grep 'Size' | cut -d' ' -f 4)";
            items=$((items + 1))
            echo "$inna"
            echo "            $(file $inna | cut --complement -d' ' -f 1 | cut -c 1-80)"
            echo "            $moo bytes"         #  remember, stat() errors are being suppressed
      mytally=$((mytally + moo))
  done </tmp/p_changes_rawlist.txt
            echo "~~~~ $mytally total BYTES (slated for sending to rootfs)"
            echo "total  $items  items changed (added|edited|deleted) since prior persist-save"
  rm /tmp/p_changes_rawlist.txt
}

asitwas () {
    restore_umount && restore_readonly   # possibly unnecessary, but no harm
}

main() {
grep -q "^PERSISTENCE=.*dynamic" /live/config/initrd.out || error_box_pf "%s was not enabled.  Cannot %s" "Dynamic root persistence" "${0##*/}"
read_conf ||    read_conf_error "bigshoe" "potato" #"rootfs"

trap asitwas EXIT
check_persist_uuid || error_box "Error when checking for persistence device"
mount_if_needed $PERSIST_DEV $PERSIST_MP
make_readwrite $PERSIST_MP
mount_if_needed -o loop $PERSIST_FILE $ROOTFS_MP
make_readwrite $ROOTFS_MP

local upper
local root_fs=$(df -T / | tail -n1 | awk '{print $2}')
[ "$root_fs" = "overlay" ] && upper="/upper"
rootfs_excludes "/${upper#/}" | sort -u > /live/config/rsync.exclude
rsync_opts="-a --dry-run -v    --delete-excluded --exclude-from=/live/config/rsync.exclude  $AUFS_RAM_MP/ $ROOTFS_MP/"
rsync -a $rsync_opts  > /tmp/p_changes.out.txt
who=$SUDO_USER
chmod 0666 /tmp/p_changes.out.txt  && chown "$who:$who" /tmp/p_changes.out.txt

sed -i "s/^sending incremental file list$/SLATED persist-save LIST OF NON-EXCLUDED CHANGED FILES\n   $(date)\n/" /tmp/p_changes.out.txt
sed -i 's/^upper//g' /tmp/p_changes.out.txt
sed -i 's/^deleting upper/   will DELETE item within the savefile   /g'  /tmp/p_changes.out.txt
sed -i 's/^work\/work\/$//'  /tmp/p_changes.out.txt   #this workdir does exist, is eternally empty, is a transient holding pen for overlayfs
sed -i 's/^\/$//'  /tmp/p_changes.out.txt    #  stray line resulting from removing "^/upper"
sed -i 's/^\/.*\/$/\n&/g' /tmp/p_changes.out.txt
sed -i -e 's/speedup.*//' -e 's/total size is.*//' -e 's/^sent .*bytes.*$//'  /tmp/p_changes.out.txt
echo "\

presence of a directory path bracketed by blank lines, e.g.
_______________________

/home/demo/.cache/
______________________
indicates that an excludes_list pattern matched all changed files residing in/under the directory" >>  /tmp/p_changes.out.txt

test $1 && showitemdetails
su $who -c 'leafpad /tmp/p_changes.out.txt&'
clean_up && trap EXIT
exit 0
} # end main

main "$@"
