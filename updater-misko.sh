#!/bin/bash
#
# Bash/YAD Debian Package Updater based on Linux Lite update tool
# License: GNU GPLv2
#	 https://web.archive.org/web/20190718231741/https://github.com/Misko-2083/lite-update-dangerzone/blob/master/LICENSE
#
# original author: copyright 2015  Milos Pavlovic (aka "misko-2083")
#	 https://web.archive.org/web/20190718231933/https://raw.githubusercontent.com/Misko-2083/lite-update-dangerzone/master/lite-update
#
# edited for antiX 19 by PPC ~~ 18/7/2019 (changed "apt update" part of the "Update" Function 
# and reduced the "Main menu" turning it into a confirmation menu only)


###				copypaste to/from webpages
###				(especially pages like the Wordpress-powered antixforum pages!)
###				is prone to introducing errors


###		tailend of a script should be a blank line


###		Will the user understand "purging the cache" ?
###		??? "purging the apt cache"
###		??? "cleanup ~~ removal of downloaded .deb packages which have been installed"

###     /etc/apt/sources.list <<<<<<<<<<<<<<<<< deprecated

###   verbiage: What happens next        Checking availability of upgradable packages.




APPNAME="_"  ### keep the displayed name generic, avoid translation chore
LOGFILE=/var/log/pkg-updater-misko.log
UPDATELOG=/var/log/prior-pkgupdate.log
### dist-upgrade or upgrade, change next line
COMMAND=dist-upgrade

### ensure only root can run this script
if [[ $EUID -ne 0 ]]; then
	echo -e "This script must be run as root!   \nTIP:\nUse \n    sudo $0\nor\n    gksu $0" 1>&2
	###   skidoo sez: gksu is overused; zero benefit in choosing it instead of sudo here
	###    ( and FYI "gksudo" is just an alias for gksu, along with {in antiX17/19} su-to-root )
	###                    v---beginning of line spaces for indentation 
	yad --center --info  --button="OK":0 \
	    --text="\n This package updater utility must be run as root!\n\nTIP:\n  Use\n     sudo $0 \n  or\n     gksu $0"
	exit 1
fi

function log(){
	message="$@"
	echo '['$(date +%D\ %H:%M:%S)'] '$message | sed -r "s/\x1B\[([0-9]{1,2}(;[0-9]{1,2})*)?m//g" | fold -sw 76 >>$LOGFILE
}

function checklock () {
### Check for exclusive lock and wait until package managers are closed
while  fuser /var/lib/dpkg/lock 2>&1 >/dev/null  ; do
	echo -e "# Waiting for other software managers to finish...\n"
	if [ ! -z "$(pgrep synaptic)" ] || [ ! -z "$(pgrep packageinstaller)" ] || \
	   [ ! -z "$(pgrep aptitude)" ] || [ ! -z "$(pgrep gdebi-gtk)" ]        || \
	   [ ! -z "$(pgrep apt)"      ] || [ ! -z "$(pgrep dpkg)" ]; then
		echo -e "# Waiting for other software managers to finish...\n"
	fi
	echo 1
	sleep 1
	echo 30
	sleep 1
	echo 60
	sleep 1
	if [ ! -z "$(pgrep gdebi-gtk)" ]; then
		echo "# Waiting for other software managers to finish..."  ### idunno. gdebi is a slowpoke? does not use dpkg lock?
	fi
	echo 95
	sleep 1
done | (if ! yad --progress --center --text="batman sez\n  ^.|.^" --title="!!! close any running package managers !!!" --percentage=20 \
        --width=420 --auto-close --button="Cancel:1";then killall $(basename $0); exit; fi)
}

function update() {  ### MANY LINES, SO SKIP ONEL LEVEL OF INDENTATION

TMPLIST=$(mktemp /tmp/repos.XXXXXX) ### Temporary file for error tracking

# Test for network conection
###  "a few minutes" will be FOREVER if no network connection, eh?
###
###  TODO: test network availability, generic
###             timeout 5 ping -q -w 1 -c 1 www.amazon.com > /dev/null && echo ok || echo error
###
###        create an array of the enabled apt server URLs, loop to test connection to each
###    grep -s -h ^deb /etc/apt/sources.list /etc/apt/sources.list.d/* | cut -d ' ' -f 2 | sed s'/\/$//'g | sort -u
###                         "sources.list" not present by default, but sysadmin may have manually created it
###                         the list may contain redundant URLs (and, some with trailing slash, others without)
###
###
###
apt-get update 2>&1 | tee $TMPLIST | yad --center --borders=15 --image=system-software-install --progress \
   --pulsate --auto-close --no-cancel --width="420" --height="80" --title="$_APPNAME" \
   --title="$APPNAME" --text="Checking availability of upgradable packages.\nPlease wait, this may take a few minutes...\n" \
   --text-align center --no-buttons --skip-taskbar 2>/dev/null

if [ "${PIPESTATUS[0]}" -ne "0" ]; then
	err_msg=$(awk '/^(W:|E:)/{$1=""; print $0}' $TMPLIST | tail -n 1 )	#Get errors/warnings
	log "ERROR: $err_msg"
	rm -f $TMPLIST
	unset TMPLIST
	yad --error --center --title="Error" --text="could not fetch packages list from repositories.\nCheck the log for details"
	return
fi

log "INFO: retrieved package manifest list(s)"
rm -f $TMPLIST
unset TMPLIST

UPDATES=$(mktemp /tmp/updateslist.XXXXXX)
IMACOPY=$(mktemp /tmp/imacopy.XXXXXX)

###  Creates a list in /tmp/updateslist
apt-get --just-print $COMMAND 2>&1 | perl -ne \
   'if (/Inst\s([\w,\-,\d,\.,~,:,\+]+)\s\[([\w,\-,\d,\.,~,:,\+]+)\]\s\(([\w,\-,\d,\.,~,:,\+]+)\)? /i) {print "Name: $1 ___ INSTALLED: $2 ___ AVAILABLE: $3\n"}' \
   | awk '{print NR,":\t"$0}' \
   | tee $UPDATES  | yad --progress --center --pulsate --title="Calculating Updates" --text="Please wait..." --auto-close

###  Check if any updates are available, if there are none, script pops up dialog box saying 'No Updates Available', removes /tmp/updateslist.XXXXXX
if [  -z "$(cat $UPDATES)"  ]; then
	log "INFO: No updates are available."
	rm -f $UPDATES
	unset UPDATES
	yad --info --title="$APPNAME" --width=300 --center --text="No Updates Available" --button="OK":0
	exit
	###  If there are no updates, there's no point returning to the main menu
	#return
fi

### Log the available update items
lst_upgrades=$(awk 'BEGIN { FS = "[ ]" } { print $3 }' $UPDATES)
log "INFO: Updates available: $lst_upgrades"
unset lst_upgrades

cp -f $UPDATES $IMACOPY  ### here, we do not yet have the intro "List of available..." header lines
cut -d ':' -f 3-7 $IMACOPY | sort > $UPDATES    # each line does not have 7 fields, but hey
rm -f $IMACOPY
unset IMACOPY

#read -p "__paused__"   ### (useful during debugging)

###	TODO
###		consider checking $(apt-mark showhold)
###		and if the list is non-empty, display a followup dialog
###		reminding which items are currently pinned


### Insert text into  /tmp/updateslist.XXXXXX
sed -i -e '1 i\List of available Updates' -e '1 i\Click Update to continue, or Cancel to stop the update process\n'  $UPDATES

### Erase existing available info
sudo dpkg --clear-avail

### Call the yad dialog to show update list (mention TIP in case user has doubt/question and wants to paste into forum post and inquire)
yad --text-info --center --button="Update:0" --button="Cancel:1" --title="Available Updates" --width=780 --height=540 --filename="$UPDATES" \
    --text="     TIP: you can select-n-copy some, or all, of the info displayed below"

###  Continue script if no halt
if [ "$?" -eq "0" ];then
	### Write log
	log "INFO: scripted package update operation has started."

	### Remove tmp file and unset variable
	rm -f $UPDATES
	unset UPDATES

	### Begin upgrade
	DEBIAN_FRONTEND=noninteractive apt-get  -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" $COMMAND \
	    --show-progress -y  2>&1 | tee $UPDATELOG | awk ' BEGIN { FS=" "; total=1;end_download=0} /upgraded/ {total= $1 + $3;FS="[ :]" } /^Get:[[:digit:]]+/ {printf "#Downloading %s %s %s\n",$7,$(NF-1),$NF;print int(($2 - 1) * 100 / total); fflush(stdout)} /^\(Reading / {if (end_download==0){print 100;fflush(stdout);end_download=1}} /^(Preparing|Unpacking|Selecting|Processing|Setting|Download)/ {print "#", $0; fflush(stdout)}  /^Progress/ {print  match($0, /([0-9]+)/, arr); if(arr[1] != "") print arr[1] ; fflush(stdout)}' \
	    | ( yad --progress --center --width=600 --text="Downloading package(s)...\nThis may take a while." \
	         --title="Downloading - please wait..." --percentage=0 --auto-close ; yad --progress --center --width=600 \
	         --text="Installing and configuring packages...\nThis may take a while." --title="Installing - please wait..." --auto-close )

	if [ "${PIPESTATUS[0]}" -ne "0" ]; then
		err_msg=$(awk '/^(W:|E:)/{$1=""; print $0}' $UPDATELOG | tail -n 1)
		log "ERROR:$err_msg"
		yad --error --center --title="Error" --text="The update operation has failed.\nCheck the log for details.\n\n$LOGFILE"
		exit ######### return                ######   TELL THE USER WHERE IS THE LOG
	fi

### Halt updates script if user selects Cancel
else
	log "INFO: User has canceled the software upgrades script."  ### $0
	rm -f $UPDATES
	unset UPDATES
	exit ######### return
fi

log "INFO: Updates successfully installed (via scripted dist-upgrade operation)"
PROCEED=$(yad --question --title="$APPNAME" --center --text="Updates have finished installing.\n\nWould you like to view the log?"; echo $?)

if [ ${PROCEED} -eq 1 ]; then
    yad --info --center --title="$APPNAME" --text="Updates completed successfully"
    return;
else
    yad --text-info --center --ok-label="Quit" --cancel-label="Cancel" --title="Updates Log" --width=700 --height=480 --filename="$UPDATELOG"
fi

return
}   ### END OF update() FUNCTION


### Start the main loop
while (true); do
	selection=$(yad --list --radiolist --title="$APPNAME" --text="Please confirm" --height=150 --width=420 \
	   --hide-column=2 --button=gtk-no:1 --button=gtk-yes:0 --column="" --column="Function" --center \
	   --column="" "TRUE" "update" " Update Packages    (apt $COMMAND)"  --print-column=2 \
	   --text="Update packages\n\n If you click YES, a list of upgradable packages will be displayed and you will be asked whether or not you wish to proceed with the update operation.") || exit
	selection=$(echo $selection | cut -d '|' -f 1)

	case "$selection" in
		update) checklock; update
		;;
		showlog) showlog
		;;
		purgecache) checklock; sudo rm -vfd /var/lib/apt/lists/*  || yad --error --center --text="Error while purging the apt cache\nTry doing it manualy sudo rm /var/lib/apt/lists/* -vf"
		;;
		about) about
		;;
		exit) exit 0
		;;
	esac
done
