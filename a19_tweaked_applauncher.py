#!/usr/bin/env python
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GObject, GLib, Gio, GdkPixbuf
from xdg.DesktopEntry import DesktopEntry
import os
import re
'''
           desktop-defaults-run -t (and the -e variant) is a poor//inefficent construct.
           It causes an extra process to be spawned, and held open, for the duration of the launched process.
           Each instance, each usage, unnecessarily adds 5MB to the session overhead.

           CONSIDER   THE JOYS FROM ABIDING "XDG SPECS"
              1) USER INSTALLS "xfce4-screenshooter"
              2) ITS .desktop FILE CONFOUNDINGLY STATES "OnlyShowIn=Xfce"
              3) USER VISITS THIS APPLAUNCHER UTIL AND WONDERS "Why is xfce4-screenshooter not shown as available?"
              4) USER POSTS FORUM HELP REQUEST (ER, "BUG REPORT")
'''
apps = Gio.app_info_get_all()

class mainWindow(Gtk.Window):

    def refresh_filter(self,widget):
        self.filter.refilter()
        search_query = self.searchentry.get_text()
        if search_query != "":
            self.treeview.set_cursor(0) # howdy    skip this when (user has changed selectbox choice and) entrybox is blank

    def resetsearch(self,widget):
        self.searchentry.set_text("")
        self.treeview.set_cursor(0)  # howdy    really, at this juncture, NONE shold hold focus (this line just intended to trigger scroll-to-top)
        tree_selection = self.treeview.get_selection()
        tree_selection.unselect_all()
        self.refresh_filter(self)
        self.searchentry.grab_focus()

    def visible_cb(self, model, iter, data=None):
        search_query = self.searchentry.get_text().lower()
        active_field = self.searchcombo.get_active()
        search_in_all_columns = active_field == 0
        if search_query == "":      return True

        if search_in_all_columns:
            for col in range(1,self.treeview.get_n_columns()-1):  # ignore last column, it is bool
                value = model.get_value(iter, col).lower()
                if value.startswith(search_query):    return True # HOWDY   we can do better than "startswith" eh

            return False

        if active_field == 1:    # Name
            value = model.get_value(iter, 2).lower() # howdy   yeah, could just use 'active_column+1'
        elif active_field == 2:  # Description
            value = model.get_value(iter, 3).lower()
        elif active_field == 3:  # Exec
            value = model.get_value(iter, 4).lower()

        return True if value.startswith(search_query) else False

    def run_button(self, test):
        tree_selection = self.treeview.get_selection()
        self.run(self,"","")

    def run(self, test, fill, fill2):
        tree_selection = self.treeview.get_selection()
        (model, pathlist) = tree_selection.get_selected_rows()
        for i, path in enumerate(pathlist) :
            tree_iter = model.get_iter(path)
            appname = model.get_value(tree_iter,2)
            appexec = model.get_value(tree_iter,4)
            boolterm = model.get_value(tree_iter,5)

            #firstchunk = str(appexec.split(' ', 1)[0])
            #wecan = os.access(firstchunk, os.X_OK)   ### not working (fallback to at least checking it exists?)
            wecan = True    #############
            if wecan == True:
                #print "target exists and is executable by the user"
                appexec = re.sub(r'%.*', '', appexec)
                if boolterm == True:
                    os.system("lxterminal --title '...terminal emulator window launched via All Applications' -e " + appexec + " &")
                else:
                    os.system(appexec + " &")
            else:
                ouch = "Error!\n\n" + firstchunk + "\nis missing or is not readable"
                msg_dialog = Gtk.MessageDialog(self, Gtk.DialogFlags.MODAL, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK)
                msg_dialog.set_title("_")  #  avoid   Unnamed
                msg_dialog.format_secondary_text(ouch)
                msg_dialog.run()
                msg_dialog.destroy()

    def make_list(self):
        store = Gtk.ListStore(GdkPixbuf.Pixbuf,str,str,str,str,bool)
        for name in apps:
            filename = name.get_filename()
            appname = DesktopEntry(filename).getName()
            appdesc = DesktopEntry(filename).getComment()
            if appdesc == "":
                appdesc = "  ~~~" # provide visual continuity between the name and its associated execstring
            appexec = DesktopEntry(filename).getExec()
            appneedterm = DesktopEntry(filename).getTerminal()
            appconcat = "\n" + DesktopEntry(filename).getName() + "\n               " + \
                    DesktopEntry(filename).getComment() + "\n               Exec= " + DesktopEntry(filename).getExec() + "\n"
            appicon = DesktopEntry(filename).getIcon()
            if appicon == "":
                #appicon = "error"  # howdy    hopefully not a RedX, eh (ah, indeed, a RedX speech bubble was displayed for 'bash /usr/bin/bc')
                appicon = "gtk-dialog-info"  # idunno which other suitably-generic icon we can trust will be present

            if os.path.exists(appicon):
                try:
                    pixbuf = GdkPixbuf.Pixbuf.new_from_file(appicon)
                    # howdy    added try   due to the following intermittent error
                    #    GLib.Error: gdk-pixbuf-error-quark: Image file 'galculator' contains no data (0)
                except:
                    pass
            else:
                icon_theme = Gtk.IconTheme.get_default()
                icon_check = icon_theme.lookup_icon(appicon, 48, 0) # howdy    smaller items look crap when stretched to 48px
                if icon_check:
                    icon_info = icon_theme.lookup_icon(appicon, 48, 0)
                    pixbuf = GdkPixbuf.Pixbuf.new_from_file(icon_info.get_filename())

            pixbuf = GdkPixbuf.Pixbuf.scale_simple(pixbuf, 48,48,0)
            store.append([pixbuf, appconcat, appname, appdesc, appexec, appneedterm])
            store.set_sort_column_id(2,0)

        self.filter = store.filter_new()
        self.filter.set_visible_func(self.visible_cb)
        self.searchentry.connect("changed", self.refresh_filter)
        self.searchentry.connect("activate", self.run_button)
        self.searchcombo.connect("changed", self.refresh_filter)

        self.treeview = Gtk.TreeView.new_with_model(self.filter)
        self.treeview.set_rules_hint(True) # howdy     ineffectual
        self.treeview.set_headers_visible(False)
        self.treeview.set_enable_search(False) # howdy   without this set false, "Type to search..." does not have the intended effect, eh

        self.treeview.connect("row-activated", self.run)
        renderer = Gtk.CellRendererPixbuf()
        column = Gtk.TreeViewColumn("", renderer, pixbuf=0)
        column.set_resizable(True)
        self.treeview.append_column(column)
        for i, column_title in enumerate(["Concat", "Name", "Description", "Exec", "NeedTerm"]):
            renderer = Gtk.CellRendererText()
            column = Gtk.TreeViewColumn(column_title, renderer, text=i+1)
            column.set_resizable(True)
            if i >= 1:       column.set_visible(False)
            self.treeview.append_column(column)

        self.sw.add(self.treeview)


    def __init__(self):
        Gtk.Window.__init__(self)
        self.set_size_request(620,480)
        self.set_position(Gtk.WindowPosition.CENTER)
        self.set_border_width(8)
        self.set_title(" All Applications ")

        vbox = Gtk.VBox()
        self.add(vbox)
        hbox = Gtk.HBox()
        vbox.pack_start(hbox, 0,0,0)
        label = Gtk.Label("Search / Filter: ")
        hbox.pack_start(label,0,0,0)

        fields = ["fields:All", "Name", "Description", "Exec"]
        self.searchcombo = Gtk.ComboBoxText()
        self.searchcombo.set_entry_text_column(0)
        hbox.pack_start(self.searchcombo, False, False, 0)
        for field in fields:
            self.searchcombo.append_text(field)
        self.searchcombo.set_active(0)
        ###  TODO    howdy    begs a visual cue to indicate filtered view is in effect (e.g. colorize entrybox bg while non-blank)
        #   https://stackoverflow.com/questions/39065408/best-way-to-set-entry-background-color-in-python-gtk3-and-set-back-to-default

        self.searchentry = Gtk.Entry()
        self.searchentry.set_placeholder_text("Type to filter...")
        hbox.pack_start(self.searchentry, True, True, 0)

        self.clearbtn = Gtk.Button("clear")
        self.clearbtn.connect("clicked",self.resetsearch)
        hbox.pack_start(self.clearbtn, False, False, 0)

        self.sw= Gtk.ScrolledWindow()
        #self.sw.set_policy(Gtk.PolicyType.NEVER,Gtk.PolicyType.AUTOMATIC)
        #   ^-->   howdy     on second thought... naw
        vbox.pack_start(self.sw,1,1,1)

        self.make_list()

        buttonbox = Gtk.HButtonBox()
        vbox.pack_start(buttonbox,0,0,1)

        run = Gtk.Button(stock=Gtk.STOCK_EXECUTE)
        run.connect("clicked", self.run_button)
        buttonbox.pack_start(run,0,0,0)
        run.set_can_default(True)
        run.grab_default()

        close = Gtk.Button(stock=Gtk.STOCK_CLOSE)
        close.connect("clicked", lambda w: Gtk.main_quit())
        buttonbox.add(close)
        self.show_all()
        self.searchentry.grab_focus() # howdy   this results in absence of "Type to filter..." default text

def main():      #   v---- HOWDY   yes, this is brittle!
    if os.access("/usr/bin/lxterminal", os.X_OK) == False:
        ouch = "before attempting to use this\n'All Programs' launcher utility, please\n\n" + \
             "sudo apt install lxterminal\n\n"
        msg_dialog = Gtk.MessageDialog(None, Gtk.DialogFlags.MODAL, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK)
        msg_dialog.set_title("_Error_")
        msg_dialog.set_position(Gtk.WindowPosition.CENTER)
        msg_dialog.format_secondary_text(ouch)
        msg_dialog.run()
        msg_dialog.destroy()
        os.exit(1)

    win = mainWindow()
    win.connect("delete-event", Gtk.main_quit)
    win.connect("destroy", Gtk.main_quit)
    win.show_all()
    Gtk.main()

if __name__ == "__main__":
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL) # without this, Ctrl+C from parent term is ineffectual
    main()
